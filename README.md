README for tamdesk
==========================
Instructions for MAC users:

Download  mysql
Run the DDL sql file to create the DB with data

Download source code and compile using the following command : mvn spring-boot:run

Go to : http://localhost:8080/tamdesk.html

You might have to delete the following tables :

SET FOREIGN_KEY_CHECKS = 0;
drop table T_AUTHORITY,T_PERSISTENT_AUDIT_EVENT_DATA, T_PERSISTENT_TOKEN,T_USER_AUTHORITY,t_user,t_persistent_audit_event,oauth_access_token,oauth_approvals, oauth_client_details,oauth_client_token,oauth_code,oauth_refresh_token,DATABASECHANGELOG,DATABASECHANGELOGLOCK; 
SET FOREIGN_KEY_CHECKS = 1;
