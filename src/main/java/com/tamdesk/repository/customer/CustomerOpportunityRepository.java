        package com.tamdesk.repository.customer;

        import com.tamdesk.domain.customer.Customer_Opportunity;
        import org.springframework.data.domain.Page;
        import org.springframework.data.domain.Pageable;
        import org.springframework.data.jpa.repository.JpaRepository;
        import org.springframework.data.jpa.repository.Query;
        import java.util.List;


        /**
 * Created by psanghavi on 10/5/14.
 */
public interface CustomerOpportunityRepository extends JpaRepository<Customer_Opportunity, String> {


    @Query("select u from Customer_Opportunity u where u.id = ?1")
    public Customer_Opportunity getOpportunityById(String opportunityId);

    @Query("select u from Customer_Opportunity u where u.salesforceAccountId = ?1")
    public Page<Customer_Opportunity> getOpportunityBySalesForceId(String salesforceAccountId, Pageable pageable);

    @Query("Select u from Customer_Opportunity u where u.customer.customer_id = ?1")
    public Page<Customer_Opportunity> getOpportunities(Integer tamdeskAccountId, Pageable pageable);

    @Query("Select u from Customer_Opportunity u where u.customer.customer_id = ?1 and u.Opportunity_stage like '%Won%'")
    public List<Customer_Opportunity> getOpportunities(Integer tamdeskAccountId);
}


