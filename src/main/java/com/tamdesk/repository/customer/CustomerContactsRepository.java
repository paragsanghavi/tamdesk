package com.tamdesk.repository.customer;

import com.tamdesk.domain.customer.Customer;
import com.tamdesk.domain.customer.Customer_Contacts;
import com.tamdesk.domain.customer.Customer_Contacts_Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by psanghavi on 10/5/14.
 */
public interface CustomerContactsRepository extends JpaRepository<Customer_Contacts, String> {


    @Query("select u from Customer_Contacts u where u.id = ?1")
    public Customer_Contacts getCustomerByCustomerId(String contactId);

    @Query("select u from Customer_Contacts u where u.salesforceAccountId = ?1")
    public Page<Customer_Contacts> getCustomerBySalesForceId(String salesforceAccountId,Pageable pageable);

    @Query("Select u from Customer_Contacts u where u.tamdeskAccountId = ?1")
    public Page<Customer_Contacts> getCustomerContacts(String tamdeskAccountId, Pageable pageable);

    @Query("select u from Customer_Contacts u where u.customer.customer_id = ?1")
    List<Customer_Contacts> getCustomerContactsByCustomerId(Integer customerId);
}

