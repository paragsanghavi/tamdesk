package com.tamdesk.repository.customer;

import com.tamdesk.domain.customer.Customer;
import com.tamdesk.domain.notes.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;


import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by psanghavi on 9/28/14.
 */
public interface CustomerRepository  extends JpaRepository<Customer, String> {


    @Query("select u from Customer u where u.id = ?1")
    Customer getCustomerByCustomerId(Integer customerId);

    @Query("select u from Customer u where u.salesForceId = ?1")
    Customer getCustomerBySalesForceId(String salesForceId);

    @Query(value="select * from tamdesk_customer u order by CONVERT(u.Total_Account_Revenue,DECIMAL) desc limit 10 ", nativeQuery = true)
    List<Customer> top10CustomerByRevenue( );

    @Query(value = "select * from tamdesk_customer u order by u.adoptionScore desc limit 10 ", nativeQuery = true)
    List<Customer> top10CustomerByAdoptionScore( );

    @Query(value= "select * from tamdesk_customer u order by u.riskScore desc limit 10 ", nativeQuery = true)
    List<Customer> top10CustomerByRiskScore( );

    @Query("Select count(*) from Customer ")
    long getCount( );

    @Query("Select count(*) from Customer u where u.customer_region=?1 ")
    long getCustomerCountByRegion(String Region );

    @Query("Select u from Customer u")
    public Page<Customer> getCustomers(Pageable pageable);

    @Query("Select u from Customer u where u.Customer_Relationship_manager is NULL")
    public Page<Customer> getOrphanedCRMAccounts(Pageable pageable);

    @Query("Select u from Customer u where u.Technical_Account_Manager is NULL")
    public Page<Customer> getOrphanedTAMAccounts(Pageable pageable);

    @Query("Select u from Customer u")
    List<Customer> getallCustomers( );



    /*
    @Query(value = "select no.* from NOODLE_ORDERS no where no.ORDER_ID in (select ID from ORDER_ORDER_ITEMS where MENU_ID = :menuId)", nativeQuery = true)
    List<Order> findOrdersContaining(@Param("menuId") String menuId);
    */
}
