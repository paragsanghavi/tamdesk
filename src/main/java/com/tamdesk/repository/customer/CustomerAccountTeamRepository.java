package com.tamdesk.repository.customer;

import com.tamdesk.domain.customer.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.tamdesk.domain.customer.Customer_Account_Team;

/**
 * Created by psanghavi on 9/28/14.
 */
public interface CustomerAccountTeamRepository extends JpaRepository<Customer, String> {


   @Query(value="select m.customer_id, m.name,\n" +
           "(select u1.name from tamdesk_user as u1 where m.salesAccountMgr = u1.salesforceId) as salesAccountMgr,\n" +
           "(select u2.name from tamdesk_user as u2 where m.Technical_Account_Manager = u2.salesforceId) as Technical_Account_Manager,\n" +
           "(select u3.name from tamdesk_user as u3 where m.Customer_Relationship_manager = u3.salesforceId) as Customer_Relationship_manager,\n" +
           "(select u4.name from tamdesk_user as u4 where m.Technical_Architect = u4.salesforceId) as Technical_Architect,\n" +
           "(select u5.name from tamdesk_user as u5 where m.Secondary_Technical_Account_Manager = u5.salesforceId) as Secondary_Technical_Account_Manager,\n" +
           "(select u6.name from tamdesk_user as u6 where m.ExecutiveSponsor = u6.salesforceId) as ExecutiveSponsor,\n" +
           "(select u7.name from tamdesk_user as u7 where m.salesEngineer = u7.salesforceId) as salesEngineer,\n" +
           "(select u8.name from tamdesk_user as u8 where m.TechChampion = u8.salesforceId) as TechChampion\n" +
           "from  tamdesk_customer as m where m.customer_id=?1", nativeQuery = true)
   public Customer_Account_Team getAccountTeam(Integer customerId);



}
