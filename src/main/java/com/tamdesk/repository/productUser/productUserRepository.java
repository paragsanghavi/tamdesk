package com.tamdesk.repository.productUser;


import com.tamdesk.domain.productuser.productUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

/**
 * Created by psanghavi on 10/5/14.
 */
public interface productUserRepository extends JpaRepository<productUser, String> {

    @Query("select u from productUser u where u.id = ?1")
    public productUser getUserById(String userId);

    @Query("select u from productUser u where u.salesforceAccountId = ?1")
    public Page<productUser> getproductUsersBySalesForceAccountId(String salesforceAccountId,Pageable pageable);

    @Query("Select u from productUser u where u.tamdeskAccountId = ?1")
    public Page<productUser> getAllProductUsersByTamDeskID(String tamdeskAccountId, Pageable pageable);

    @Query("Select u from productUser u" )
    public List<productUser> getAllProductUsers();
}
