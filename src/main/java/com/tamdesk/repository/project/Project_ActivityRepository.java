package com.tamdesk.repository.project;

import com.tamdesk.domain.project.Project;
import com.tamdesk.domain.project.Project_Activity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.joda.time.LocalDateTime;

import java.util.List;

/**
 * Created by psanghavi on 10/8/14.
 */
public interface Project_ActivityRepository extends JpaRepository<Project_Activity, String> {

        @Query("select u from Project_Activity u where u.activity_id = ?1")
        Project_Activity getProjectActivityByActivityId(String activity_id);

        @Query("select u from Project_Activity u where u.project.id = ?1")
        List<Project_Activity>  getProjectActivityByProjectId(Integer project_id);


        @Query("Select count(*) from Project_Activity ")
        long getCount( );

        //This will get us the last week of activities that has to be reported to finance or management team
        @Query(value= "SELECT u.* FROM Project_Activity u WHERE activity_end_date >= curdate() - INTERVAL DAYOFWEEK(curdate())+6 DAY AND activity_end_date < curdate() - INTERVAL DAYOFWEEK(curdate())-1 DAY", nativeQuery = true)
        public List<Project_Activity> getProjectActivities_for_last_week();

        @Query( "SELECT u FROM Project_Activity u WHERE u.activity_end_date BETWEEN ?1 AND ?2")
        public Page<Project_Activity> getProjectActivities_in_date_range(LocalDateTime start_date, LocalDateTime end_date, Pageable pageable);


}
