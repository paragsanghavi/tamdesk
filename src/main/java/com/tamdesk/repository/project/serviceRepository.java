package com.tamdesk.repository.project;

import com.tamdesk.domain.project.Services;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by psanghavi on 11/30/14.
 */
public interface serviceRepository extends JpaRepository<Services, String> {
    @Query("select u from Services u where u.id = ?1")
    Services getserviceByServiceId(Integer service_id);
}
