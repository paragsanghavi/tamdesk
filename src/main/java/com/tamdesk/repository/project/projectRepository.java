package com.tamdesk.repository.project;


import com.tamdesk.domain.project.Project;
import com.tamdesk.domain.project.Service_Packages;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by psanghavi on 10/8/14.
 */
public interface projectRepository extends JpaRepository<Project, String> {

    @Query("select u from Project u where u.id = ?1")
    Project getProjectByProjectId(Integer project_id);

    @Query("select u from Project u where u.id = ?1")
    Project getProjectByProjectId(String project_id);

    @Query("select u from Project u where u.customer.customer_id = ?1")
    List<Project> findProjectsByCustomerId(Integer customerId);

    @Query("select u from Project u where u.customer.customer_id = ?1")
    List<Project> findProjectsByCustomerId(String customerId);

    @Query("Select count(*) from Project ")
    long getCount( );

    @Query("Select u from Project u")
    public Page<Project> getProjects(Pageable pageable);





}
