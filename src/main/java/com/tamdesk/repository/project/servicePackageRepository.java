package com.tamdesk.repository.project;

import com.tamdesk.domain.project.Service_Packages;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by psanghavi on 12/1/14.
 */
public interface servicePackageRepository extends JpaRepository<Service_Packages, String> {

    @Query("select u from Service_Packages u ")
    List<Service_Packages> getallServiceProducts( );

}
