package com.tamdesk.repository.notes;

import com.tamdesk.domain.notes.Note;
import org.joda.time.DateTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by psanghavi on 9/23/14.
 */
public interface NoteRepository extends JpaRepository<Note, String> {

    @Query("select u from Note u where u.id = ?1")
    Note getNoteByNoteId(String noteId);

    @Query("select u from Note u where u.id = ?1")
    Note getNoteByNoteId(Integer noteId);

    @Query("select u from Note u where u.customer.customer_id = ?1")
    List<Note> findNotesByAccountId(String accountId);

    @Query("Select count(*) from Note u where u.customer.customer_id = ?1")
    long getCount(String accountId);

}
