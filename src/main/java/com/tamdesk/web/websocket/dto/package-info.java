/**
 * Data Access Objects used by WebSocket services.
 */
package com.tamdesk.web.websocket.dto;
