package com.tamdesk.web.rest.util;

/**
 * Created by psanghavi on 10/30/14.
 * I have not figured out how to transfer count of certain objects via rest. This is my mechanism to send the count object and make it
 * easier to read it on the client side
 */
public class Count {
    private Long count;

    public Count(Long count) {
        this.count = count;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
