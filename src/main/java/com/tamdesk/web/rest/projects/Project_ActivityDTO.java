package com.tamdesk.web.rest.projects;

import com.tamdesk.domain.project.*;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.joda.time.Period;

/**
 * Created by psanghavi on 12/24/14.
 */
public class Project_ActivityDTO {
    private final Logger log = LoggerFactory.getLogger(Project_ActivityDTO.class);

    private Integer activity_id;
    private Integer project_id;
    private String username;
    private Integer username_id;
    private String project_activity_type;
    private Integer project_activity_hours;
    private String project_activity_location;
    private String activity_description;
    private String activity_start_date;
    private String activity_end_date;
    private String project_activity_status;

    public Project_ActivityDTO() {
    }

    public Project_ActivityDTO(Integer activity_id, Integer project_id, String username, Integer username_id,
                               String project_activity_type, Integer project_activity_hours, String project_activity_location,
                               String activity_description, String start_date, String end_date, String project_activity_status) {
        this.activity_id = activity_id;
        this.project_id = project_id;
        this.username = username;
        this.username_id = username_id;
        this.project_activity_type = project_activity_type;
        this.project_activity_hours = project_activity_hours;
        this.project_activity_location = project_activity_location;
        this.activity_description = activity_description;
        this.activity_start_date = start_date;
        this.activity_end_date = end_date;
        this.project_activity_status = project_activity_status;
    }

    public Project_Activity toProject_Activity (){

        Date activity_start_date_dateobj=null;
        Date activity_end_date_dateobj=null;

        //We are doing this because angular sends the date in a json string format and
        //we are parsing the date string into a date object
        try{
            //_startdate 2014-11-15 00:00:00 - date format coming from angularjs front end
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            if (activity_start_date !=null)
                activity_start_date_dateobj=  sdf.parse(activity_start_date) ;

            if (activity_end_date!=null)
                activity_end_date_dateobj = sdf.parse(activity_end_date);

        }catch (Exception ex) {
            log.debug("**************Creating Project object and encounterd date parsing error, returned null project.start Date:  {}", activity_start_date);
            log.debug("**************Creating Project object and encounterd date parsing error, returned null project. end date : {}", activity_end_date);
            log.debug( ex.toString());
            return null;
        }

        Project_Activity activity = new Project_Activity();

        if(activity_id!=null) {
            activity.setId(this.getActivity_id());
        }
        Project project = new Project();

        if(project_id!=null) {
            project.setId(this.getProject_id());
            activity.setProject(project);
        }
        if ( this.getProject_activity_type() !=null)
            activity.setProject_activity_type(Project_Activity_Type.valueOf(this.getProject_activity_type()));

        if(this.getProject_activity_location()!=null)
            activity.setProject_activity_location(Project_Activity_Location.valueOf(this.getProject_activity_location()));

        if(this.getProject_activity_status()!=null)
            activity.setProject_activity_status(Project_Activity_Status.valueOf(this.getProject_activity_status()));

        activity.setActivity_description(this.getActivity_description());

        if(activity_start_date!=null)
            activity.setActivity_start_date(LocalDateTime.fromDateFields(activity_start_date_dateobj));

        if(activity_end_date!=null)
            activity.setActivity_end_date(LocalDateTime.fromDateFields(activity_end_date_dateobj));

        if(this.getUsername()!=null){
            activity.setUsername(this.getUsername());
        }
        if(this.getUsername_id()!=null){
            activity.setUsername_id(this.getUsername_id());
        }

        Period p = new Period(LocalDateTime.fromDateFields(activity_start_date_dateobj), LocalDateTime.fromDateFields(activity_end_date_dateobj));
        int hours = p.getHours();
        log.debug("*****Hours delivered:,{}",hours);

        activity.setProject_activity_hours(hours);

        return activity;
    }

    public Integer getActivity_id() {
        return activity_id;
    }

    public void setActivity_id(Integer activity_id) {
        this.activity_id = activity_id;
    }

    public Integer getProject_id() {
        return project_id;
    }

    public void setProject_id(Integer project_id) {
        this.project_id = project_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getUsername_id() {
        return username_id;
    }

    public void setUsername_id(Integer username_id) {
        this.username_id = username_id;
    }

    public String getProject_activity_type() {
        return project_activity_type;
    }

    public void setProject_activity_type(String project_activity_type) {
        this.project_activity_type = project_activity_type;
    }

    public Integer getProject_activity_hours() {
        return project_activity_hours;
    }

    public void setProject_activity_hours(Integer project_activity_hours) {
        this.project_activity_hours = project_activity_hours;
    }

    public String getProject_activity_location() {
        return project_activity_location;
    }

    public void setProject_activity_location(String project_activity_location) {
        this.project_activity_location = project_activity_location;
    }

    public String getActivity_description() {
        return activity_description;
    }

    public void setActivity_description(String activity_description) {
        this.activity_description = activity_description;
    }

    public String getActivity_start_date() {
        return activity_start_date;
    }

    public void setActivity_start_date(String activity_start_date) {
        this.activity_start_date = activity_start_date;
    }

    public String getActivity_end_date() {
        return activity_end_date;
    }

    public void setActivity_end_date(String activity_end_date) {
        this.activity_end_date = activity_end_date;
    }

    public String getProject_activity_status() {
        return project_activity_status;
    }

    public void setProject_activity_status(String project_activity_status) {
        this.project_activity_status = project_activity_status;
    }
}
