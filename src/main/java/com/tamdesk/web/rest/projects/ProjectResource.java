package com.tamdesk.web.rest.projects;

import com.codahale.metrics.annotation.Timed;
import com.tamdesk.domain.customer.Customer;
import com.tamdesk.domain.customer.Customer_Contacts;
import com.tamdesk.domain.project.Project;
import com.tamdesk.domain.project.Service_Packages;
import com.tamdesk.domain.project.Services;
import com.tamdesk.repository.project.projectRepository;
import com.tamdesk.repository.project.serviceRepository;
import com.tamdesk.repository.project.servicePackageRepository;
import com.tamdesk.web.rest.projects.projectDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by psanghavi on 10/8/14.
 */

@RestController
@RequestMapping("/tamdesk/rest")
public class ProjectResource {

    private final Logger log = LoggerFactory.getLogger(ProjectResource.class);

    @Inject
    private projectRepository projectrepo;
    @Inject
    private serviceRepository servicerepo;
    @Inject
    private servicePackageRepository   servicePackagerepo;



    /**
     * POST  create a new Project
     */


    @RequestMapping(value = "/accounts/{customerId}/project/create",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    public void createProject(@PathVariable Integer customerId, @RequestBody com.tamdesk.web.rest.projects.projectDTO projectdto) {
        log.debug("**************Creating Project for account ID {}", customerId);
        log.debug("**************Creating Project {}", projectdto.getProject_name());

        Project project =   projectdto.toProject();
        projectrepo.save(project);
        log.debug("**************Created Project {}",project.getId());

        //Now save the producs/services purchased in the Services table

        //Set the service/products purchased
        List<serviceDTO> services_dto = projectdto.getServices(); //get it from the dto first

        List<Services> services_purchased = new ArrayList<Services>();
        for (serviceDTO temp : services_dto) {

            Services service1 = new Services();
            service1.setArchitect_days(temp.getArchitect_days());
            service1.setQuantity_purchased(temp.getQuantity_purchased());
            service1.setService_days(temp.getService_days());
            service1.setPublic_training_class_seats(temp.getPublic_training_class_seats());
            service1.setPrivate_training_class(temp.getPrivate_training_class());
            service1.setSales_giveaway_days(temp.getSales_giveaway_days());
            service1.setService_warranty_days(temp.getService_warranty_days());
            service1.setServices_units(temp.getServices_units());
            service1.setProduct_warranty_days(temp.getProduct_warranty_days());
            service1.setPackage_name(temp.getPackage_name());

            //Service_Packages sp = new Service_Packages();
            //sp.setPackage_id(temp.getPackage_id());
            //service1.setPackage_purchased(sp);
            service1.setSKU(temp.getSKU());
            service1.setProject(project);

            log.debug("**************Creating services for project {}", project.getProject_name());

            servicerepo.save(service1);
            log.debug("**************Created Service {}",service1.getService_id());
        }

    }

    /**
     * Get  /accounts/{accountId}/project/new -> Create an empty project object.
     */
    @RequestMapping(value = "/accounts/{accountId}/project/new",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public Project getProject(@PathVariable Integer accountId) {
        Customer customer = new Customer();
        customer.setCustomer_id(accountId);
        Project project = new Project();
        project.setCustomer(customer);
        return project;
    }


    /**
     * Get one project details based on project Id
     */
    @RequestMapping(value = "/accounts/{accountId}/project/{projectId}}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Project getProjectByProjectId(@RequestParam(value = "projectId") String projectId) {

        Project project = projectrepo.getProjectByProjectId(projectId);
        log.debug("REST request to get the project {}", project.getProject_name());
        return project;
    }

    /**
     * Get  all projects for a customer
     */
    @RequestMapping(value = "/accounts/{accountId}/projects",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Project> findProjectsByCustomerId(@PathVariable Integer accountId) {

        List<Project> customer_projects = projectrepo.findProjectsByCustomerId (accountId );
        log.debug("REST request to get the  customer projects by tamdesk AccountId ");
        return customer_projects;
    }

    /**
     * Get  all projects in the system.
     */
    @RequestMapping(value = "/projects",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Page<Project> getProjects(Pageable pageable) {

        Page<Project> projects = projectrepo.getProjects(pageable);
        log.debug("REST request to get all the  projects ");
        return projects;
    }

    /**
     * Get  all projects in the system.
     */
    @RequestMapping(value = "/projects/getallServiceProducts",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Service_Packages> getallServiceProducts( ) {

        List<Service_Packages> service_packageses = servicePackagerepo.getallServiceProducts();
        log.debug("REST request to get all the  service Products ");
        return service_packageses;
    }
    /**
     * Get  all projects in the system.
     */
    @RequestMapping(value = "/projects/delete/{projectId}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    public void deleteProjectbyID( @PathVariable Integer projectId) {


        log.debug("Deleting project {} ",projectId );
        Project project = new Project();
        project.setId(projectId);
        projectrepo.delete(project);
        return;
    }


    @RequestMapping(value = "/accounts/{customerId}/project/update/{projectId}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    public void updateProject(@PathVariable Integer customerId,@PathVariable Integer projectId, @RequestBody com.tamdesk.web.rest.projects.projectDTO projectdto) {
        log.debug("**************Updating Project for account ID {}", customerId);
        log.debug("**************Updating Project {}", projectdto.getProject_name());

        Project project =   projectdto.toProject();
        projectrepo.save(project);
        log.debug("**************Created Project {}",project.getId());
    }

    }
