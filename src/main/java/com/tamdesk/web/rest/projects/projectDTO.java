package com.tamdesk.web.rest.projects;

import com.tamdesk.domain.customer.Customer;
import com.tamdesk.domain.project.*;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.joda.time.LocalDateTime;
import java.util.Iterator;
import java.util.ArrayList;

/**
 * Created by psanghavi on 11/27/14.
 */
public class projectDTO {
    private final Logger log = LoggerFactory.getLogger(projectDTO.class);


    private Integer id;

    private Customer customer;

    private  Integer tamdesk_opportunity_id;

    private String salesforceAccountId;

    private  String salesforce_opportunity_id;

    private String project_name;

    private String description;

    private String projectcol;

    private String Status;

    private String project_adoption_status;

    private ProjectType project_type;

    private String project_close_date;

    private String project_extension_date;

    private String start_date;

    private String  end_date;

    private String  transition_meeting_date;

    private String  kickoff_date;

    private String  architecture_start_date;

    private String  sixty_day_EBMeeting_date;

    private String  one_twenty_day_EBMeeting_date;

    private String  one_eighty_day_EBMeeting_date;

    private String  project_type_id;
    private String  project_status_id;
    private Double  project_adoption_score;

    private String GoogleDriveURL;

    private String SalesForceURL;

    private Boolean  IsNonStandardTermsandConditions;
    private Boolean  IsEBMeetingScheduled;
    private List<serviceDTO> services;

    private List<Project_Activity> project_activities;

    public projectDTO() {
    }

    public projectDTO(Integer id, Customer customer, Integer tamdesk_opportunity_id, String salesforceAccountId,
                      String salesforce_opportunity_id, String project_name, String description, String projectcol,
                      String status, String project_adoption_status, ProjectType project_type,
                      String project_close_date, String project_extension_date, String start_date, String end_date,
                      String transition_meeting_date, String kickoff_date, String architecture_start_date,
                      String sixty_day_EBMeeting_date, String one_twenty_day_EBMeeting_date, String one_eighty_day_EBMeeting_date,
                      String project_type_id, String project_status_id, Double project_adoption_score, String googleDriveURL,
                      String salesForceURL, Boolean isNonStandardTermsandConditions, Boolean isEBMeetingScheduled) {
        this.id = id;
        this.customer = customer;
        this.tamdesk_opportunity_id = tamdesk_opportunity_id;
        this.salesforceAccountId = salesforceAccountId;
        this.salesforce_opportunity_id = salesforce_opportunity_id;
        this.project_name = project_name;
        this.description = description;
        this.projectcol = projectcol;
        this.Status = status;
        this.project_adoption_status = project_adoption_status;
        this.project_type = project_type;
        this.project_close_date = project_close_date;
        this.project_extension_date = project_extension_date;
        this.start_date = start_date;
        this.end_date = end_date;
        this.transition_meeting_date = transition_meeting_date;
        this.kickoff_date = kickoff_date;
        this.architecture_start_date = architecture_start_date;
        this.sixty_day_EBMeeting_date = sixty_day_EBMeeting_date;
        this.one_twenty_day_EBMeeting_date = one_twenty_day_EBMeeting_date;
        this.one_eighty_day_EBMeeting_date = one_eighty_day_EBMeeting_date;
        this.project_type_id = project_type_id;
        this.project_status_id = project_status_id;
        this.project_adoption_score = project_adoption_score;
        this.GoogleDriveURL = googleDriveURL;
        this.SalesForceURL = salesForceURL;
        this.IsNonStandardTermsandConditions = isNonStandardTermsandConditions;
        this.IsEBMeetingScheduled = isEBMeetingScheduled;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Integer getTamdesk_opportunity_id() {
        return tamdesk_opportunity_id;
    }

    public void setTamdesk_opportunity_id(Integer tamdesk_opportunity_id) {
        this.tamdesk_opportunity_id = tamdesk_opportunity_id;
    }

    public String getSalesforceAccountId() {
        return salesforceAccountId;
    }

    public void setSalesforceAccountId(String salesforceAccountId) {
        this.salesforceAccountId = salesforceAccountId;
    }

    public String getSalesforce_opportunity_id() {
        return salesforce_opportunity_id;
    }

    public void setSalesforce_opportunity_id(String salesforce_opportunity_id) {
        this.salesforce_opportunity_id = salesforce_opportunity_id;
    }

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProjectcol() {
        return projectcol;
    }

    public void setProjectcol(String projectcol) {
        this.projectcol = projectcol;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public ProjectType getProject_type() {
        return project_type;
    }

    public void setProject_type(ProjectType project_type) {
        this.project_type = project_type;
    }

    public String getProject_close_date() {
        return project_close_date;
    }

    public void setProject_close_date(String project_close_date) {
        this.project_close_date = project_close_date;
    }

    public String getProject_extension_date() {
        return project_extension_date;
    }

    public void setProject_extension_date(String project_extension_date) {
        this.project_extension_date = project_extension_date;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getTransition_meeting_date() {
        return transition_meeting_date;
    }

    public void setTransition_meeting_date(String transition_meeting_date) {
        this.transition_meeting_date = transition_meeting_date;
    }

    public String getKickoff_date() {
        return kickoff_date;
    }

    public void setKickoff_date(String kickoff_date) {
        this.kickoff_date = kickoff_date;
    }

    public String getArchitecture_start_date() {
        return architecture_start_date;
    }

    public void setArchitecture_start_date(String architecture_start_date) {
        this.architecture_start_date = architecture_start_date;
    }

    public String getSixty_day_EBMeeting_date() {
        return sixty_day_EBMeeting_date;
    }

    public void setSixty_day_EBMeeting_date(String sixty_day_EBMeeting_date) {
        this.sixty_day_EBMeeting_date = sixty_day_EBMeeting_date;
    }

    public String getOne_twenty_day_EBMeeting_date() {
        return one_twenty_day_EBMeeting_date;
    }

    public void setOne_twenty_day_EBMeeting_date(String one_twenty_day_EBMeeting_date) {
        this.one_twenty_day_EBMeeting_date = one_twenty_day_EBMeeting_date;
    }

    public String getOne_eighty_day_EBMeeting_date() {
        return one_eighty_day_EBMeeting_date;
    }

    public void setOne_eighty_day_EBMeeting_date(String one_eighty_day_EBMeeting_date) {
        this.one_eighty_day_EBMeeting_date = one_eighty_day_EBMeeting_date;
    }

    public String getProject_type_id() {
        return project_type_id;
    }

    public void setProject_type_id(String project_type_id) {
        this.project_type_id = project_type_id;
    }

    public String getProject_status_id() {
        return project_status_id;
    }

    public void setProject_status_id(String project_status_id) {
        this.project_status_id = project_status_id;
    }

    public Double getProject_adoption_score() {
        return project_adoption_score;
    }

    public void setProject_adoption_score(Double project_adoption_score) {
        this.project_adoption_score = project_adoption_score;
    }

    public String getGoogleDriveURL() {
        return GoogleDriveURL;
    }

    public void setGoogleDriveURL(String googleDriveURL) {
        GoogleDriveURL = googleDriveURL;
    }

    public String getSalesForceURL() {
        return SalesForceURL;
    }

    public void setSalesForceURL(String salesForceURL) {
        SalesForceURL = salesForceURL;
    }

    public Boolean getIsNonStandardTermsandConditions() {
        return IsNonStandardTermsandConditions;
    }

    public void setIsNonStandardTermsandConditions(Boolean isNonStandardTermsandConditions) {
        IsNonStandardTermsandConditions = isNonStandardTermsandConditions;
    }

    public List<serviceDTO> getServices() {
        return services;
    }

    public void setServices(List<serviceDTO> services) {
        this.services = services;
    }

    public List<Project_Activity> getProject_activities() {
        return project_activities;
    }

    public void setProject_activities(List<Project_Activity> project_activities) {
        this.project_activities = project_activities;
    }

    public String getProject_adoption_status() {
        return project_adoption_status;
    }

    public void setProject_adoption_status(String project_adoption_status) {
        this.project_adoption_status = project_adoption_status;
    }

    public Boolean getIsEBMeetingScheduled() {
        return IsEBMeetingScheduled;
    }

    public void setIsEBMeetingScheduled(Boolean isEBMeetingScheduled) {
        IsEBMeetingScheduled = isEBMeetingScheduled;
    }

    public Project toProject (){
        List<Services> services_purchased = new  ArrayList<Services>();
        Date project_start_date=null;
        Date project_end_date=null;
        Date project_close_date=null;
        Date project_extension_date=null;
        Date project_transition_date=null;
        Date project_kickoff_date=null;
        Date two_month_eb=null;
        Date four_month_eb=null;
        Date six_month_eb=null;

        //We are doing this becuase angular sends the date in a json string format and
        //we are parsing the date string into a date object
        try{
            //_startdate 2014-11-15 00:00:00 - date format coming from angular
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            if (getStart_date() !=null)
                project_start_date=  sdf.parse(getStart_date()) ;

            if (getEnd_date()!=null)
                project_end_date = sdf.parse(getEnd_date());

            if(getProject_close_date()!=null)
                project_close_date= sdf.parse(getProject_close_date());

            if(getProject_extension_date()!=null)
                project_extension_date = sdf.parse(getProject_extension_date());

            if(getTransition_meeting_date()!=null)
                project_transition_date= sdf.parse(getTransition_meeting_date());

            if(getKickoff_date()!=null)
                project_kickoff_date = sdf.parse(getKickoff_date());

            if(getSixty_day_EBMeeting_date()!=null)
                two_month_eb = sdf.parse(getSixty_day_EBMeeting_date());

            if(getOne_twenty_day_EBMeeting_date()!=null)
                four_month_eb= sdf.parse(getOne_twenty_day_EBMeeting_date());

            if(getOne_eighty_day_EBMeeting_date()!=null)
                six_month_eb = sdf.parse(getOne_eighty_day_EBMeeting_date());

        }catch (Exception ex) {
            log.debug("**************Creating Project object and encounterd date parsing error, returned null project.start Date:  {}", getStart_date());
            log.debug("**************Creating Project object and encounterd date parsing error, returned null project. end date : {}", getEnd_date());
            log.debug( ex.toString());
            return null;
        }

        Project project = new Project();

        if(id!=null) {
            project.setId(this.getId());
        }
        project.setProject_name(this.getProject_name());
        project.setDescription(this.getDescription());
        project.setCustomer(this.getCustomer());
        project.setGoogleDriveURL(this.getGoogleDriveURL());
        project.setIsNonStandardTermsandConditions(this.getIsNonStandardTermsandConditions());
        project.setSalesforceAccountId(this.getSalesforceAccountId());
        project.setSalesforce_opportunity_id(this.getSalesforce_opportunity_id());
        project.setTamdesk_opportunity_id(this.getTamdesk_opportunity_id());

        if ( this.getStatus() !=null)
            project.setStatus(Project_Status.valueOf(this.getStatus()));
        if (this.getProject_adoption_status()!=null)
            project.setProject_adoption_status(Project_Adoption_Status.valueOf( this.getProject_adoption_status()));

        project.setProject_adoption_score(this.getProject_adoption_score());
        project.setIsEBMeetingScheduled(this.getIsEBMeetingScheduled());

        if(project_start_date!=null)
            project.setStart_date(LocalDateTime.fromDateFields(project_start_date));

        if(project_end_date!=null)
            project.setEnd_date(LocalDateTime.fromDateFields(project_end_date));

        if(project_close_date!=null)
            project.setProject_close_date(LocalDateTime.fromDateFields(project_close_date));

        if(project_extension_date!=null)
            project.setProject_extension_date(LocalDateTime.fromDateFields(project_extension_date));

        if(project_transition_date!=null)
            project.setTransition_meeting_date(LocalDateTime.fromDateFields(project_transition_date));

        if(project_kickoff_date!=null)
            project.setKickoff_date(LocalDateTime.fromDateFields(project_kickoff_date));

        if(two_month_eb!=null)
            project.setSixty_day_EBMeeting_date(LocalDateTime.fromDateFields(two_month_eb));

        if(four_month_eb!=null)
            project.setOne_twenty_day_EBMeeting_date(LocalDateTime.fromDateFields(four_month_eb));

        if(six_month_eb!=null)
            project.setOne_eighty_day_EBMeeting_date(LocalDateTime.fromDateFields(six_month_eb));

        //Set the service/products purchased
        /*for (serviceDTO temp : services) {
            System.out.println(temp);
            Services service1 = new Services();
            service1.setArchitect_days(temp.getArchitect_days());
            service1.setQuantity_purchased(temp.getQuantity_purchased());
            service1.setService_days(temp.getService_days());
            service1.setPublic_training_class_seats(temp.getPublic_training_class_seats());
            service1.setPrivate_training_class(temp.getPrivate_training_class());
            service1.setSales_giveaway_days(temp.getSales_giveaway_days());
            service1.setService_warranty_days(temp.getService_warranty_days());
            service1.setServices_units(temp.getServices_units());
            service1.setProduct_warranty_days(temp.getProduct_warranty_days());
            service1.setPackage_name(temp.getPackage_name());

            Service_Packages sp = new Service_Packages();
            sp.setPackage_id(temp.getPackage_id());
            service1.setPackage_purchased(sp);
            service1.setSKU(temp.getSKU());
            services_purchased.add(service1);

        }   */
         return project;
    }
}
