package com.tamdesk.web.rest.projects;

import com.codahale.metrics.annotation.Timed;
import com.tamdesk.domain.project.Project;
import com.tamdesk.domain.project.Project_Activity;
import com.tamdesk.domain.project.Project_Activity_Location;
import com.tamdesk.domain.project.Project_Activity_Type;
import com.tamdesk.domain.project.Project_Activity_Status;
import com.tamdesk.repository.project.Project_ActivityRepository;
import com.tamdesk.repository.project.projectRepository;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by psanghavi on 10/8/14.
 */

@RestController
@RequestMapping("/tamdesk/rest")
public class Project_ActivityResource {

    private final Logger log = LoggerFactory.getLogger(Project_ActivityResource.class);

    @Inject
    private Project_ActivityRepository  project_activityRepository;


    /**
     * Get ProjectActivity Type
     */
    @RequestMapping(value = "/accounts/getProjectActivityType",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Project_Activity_Type> getProjectActivityType() {
            return  new ArrayList<Project_Activity_Type>( Arrays.asList(Project_Activity_Type.values()));

    }
    /**
     * Get ProjectActivity Location
     */
    @RequestMapping(value = "/accounts/getProjectActivityLocation",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Project_Activity_Location> getProjectActivityLocation() {
        return  new ArrayList<Project_Activity_Location>( Arrays.asList(Project_Activity_Location.values()));

    }
    /**
     * Get ProjectActivity status
     */
    @RequestMapping(value = "/accounts/getProjectActivityStatus",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Project_Activity_Status> getProjectActivityStatus() {
        return  new ArrayList<Project_Activity_Status>( Arrays.asList(Project_Activity_Status.values()));

    }


    /**
     * POST  create a new Project  Activity
     */

    @RequestMapping(value = "/project/createactivity/{projectId}/create",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    public void createProjectActivity( @PathVariable Integer projectId, @RequestBody Project_ActivityDTO projectactivityDTO) {
        log.debug("**************Creating Project Activity for project ID {}", projectId);
        Project_Activity activity =   projectactivityDTO.toProject_Activity();
        project_activityRepository.save(activity);

    }

    /**
     * Get  /accounts/{accountId}/project/new -> Create an empty project activity object.
     */
    @RequestMapping(value = "/accounts/{accountId}/project/create/{projectId}/new",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public Project_Activity getProject() {
        return new Project_Activity();
    }


    /**
     * Get one project details based on project activity Id
     */
    @RequestMapping(value = "/accounts/{accountId}/project/{projectId}/project_activity/{project_activity_id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Project_Activity getProjectActivityByActivityId(@RequestParam(value = "project_activity_id") String project_activity_id) {

        Project_Activity project_activity = project_activityRepository.getProjectActivityByActivityId(project_activity_id);
        log.debug("REST request to get the project activity for project name {}", project_activity.getProject().getProject_name());
        return project_activity;
    }

    /**
     * Get  all project activities for a project
     */
    @RequestMapping(value = "/project/{projectId}/project_activity/all",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Project_Activity> getProjectActivityByProjectId(@PathVariable Integer projectId) {

        List<Project_Activity> project_activities = project_activityRepository.getProjectActivityByProjectId(projectId) ;
        log.debug("REST request to get all the activities for customer project {} ", projectId);
        return project_activities;
    }

    /**
     * Get  all project activities for last week .
     */
    @RequestMapping(value = "/project_activities/last_week",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Project_Activity> getProjectActivities_for_last_week() {

        List<Project_Activity> project_activities = project_activityRepository.getProjectActivities_for_last_week();
        log.debug("REST request to get all the  projects last week");
        return project_activities;
    }

    /**
     * Get  /accounts all the project activites by pages
     */
    @RequestMapping(value = "/project_activities/time_range",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed

    public Page<Project_Activity> getProjectActivities_in_date_range(LocalDateTime start_date, LocalDateTime end_date, Pageable pageable) {
        Page<Project_Activity> project_activities = project_activityRepository.getProjectActivities_in_date_range(start_date,end_date,pageable);
        log.debug("REST request to get project activities in time range {}", start_date,end_date);
        return project_activities;
    }
}
