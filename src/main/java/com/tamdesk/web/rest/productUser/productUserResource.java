package com.tamdesk.web.rest.productUser;

import com.codahale.metrics.annotation.Timed;
import com.tamdesk.domain.productuser.productUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tamdesk.service.productUser.productUserService;

import javax.inject.Inject;
import java.util.List;
/**
 * Created by psanghavi on 10/5/14.
 */


@RestController
@RequestMapping("/tamdesk/rest")
public class productUserResource {

    private final Logger log = LoggerFactory.getLogger(productUserResource.class);

    @Inject
    private productUserService productuserService;

    /**
     * Get  /users/{userId}
     */
    @RequestMapping(value = "/users/{userId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public productUser getUserById(@RequestParam(value = "userId") String userId) {

        productUser product_user = productuserService.getUserById(userId) ;
        log.debug("REST request to get the product user {}", product_user.getName());
        return product_user;
    }

    /**
     * Get  /accounts/contacts/{salesforceAccountId}-> Get contact using salesforceAccountId Id.
     */
    @RequestMapping(value = "/users/{salesforceAccountId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Page<productUser> getproductUsersBySalesForceAccountId(@RequestParam(value = "salesforceAccountId") String salesforceAccountId,Pageable pageable) {

        Page<productUser> product_users = productuserService.getproductUsersBySalesForceAccountId(salesforceAccountId,  pageable);
        log.debug("REST request to get the  product users by salesforceaccountID ");
        return product_users;
    }

    /**
     * Get  /productUsers
     */
    @RequestMapping(value = "/page/productUsers",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Page<productUser> getAllProductUsersByTamDeskID(@RequestParam(value = "tamdeskAccountId") String tamdeskAccountId,Pageable pageable) {

        Page<productUser> product_users = productuserService.getAllProductUsersByTamDeskID(tamdeskAccountId,  pageable);
        log.debug("REST request to get the  product users by tamdeskAccountId ");
        return product_users;
    }

    /**
     * Get  /productUsers
     */
    @RequestMapping(value = "/productUsers",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<productUser> getAllProductUsers() {

        return productuserService.getAllProductUsers();
    }
}
