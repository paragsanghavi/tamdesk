package com.tamdesk.web.rest.customer;

/**
 * Created by psanghavi on 10/5/14.
 */

import com.codahale.metrics.annotation.Timed;
import com.tamdesk.domain.customer.Customer;
import com.tamdesk.domain.customer.Customer_Contacts;
import com.tamdesk.service.customer.CustomerContactService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

@RestController
@RequestMapping("/tamdesk/rest")
public class CustomerContactResource {

    private final Logger log = LoggerFactory.getLogger(CustomerContactResource.class);

    @Inject
    private CustomerContactService customercontactService;

    /**
     * Get  /accounts/contacts/{contactId}-> Get contact using contact Id.
     */
    @RequestMapping(value = "/accounts/contacts/{contactId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Customer_Contacts getCustomerByID(@RequestParam(value = "contactId") String contactId) {

        Customer_Contacts customer_contact = customercontactService.getCustomerByCustomerId(contactId);
        log.debug("REST request to get the customer {}", customer_contact.getName());
        return customer_contact;
    }

    /**
     * Get  /accounts/contacts/{salesforceAccountId}-> Get contact using salesforceAccountId Id.
     */
    @RequestMapping(value = "/accounts/contacts/{salesforceAccountId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Page<Customer_Contacts> getCustomerBySalesForceId(@RequestParam(value = "salesforceAccountId") String salesforceAccountId,Pageable pageable) {

        Page<Customer_Contacts> customer_contacts = customercontactService.getCustomerBySalesForceId(salesforceAccountId,  pageable);
        log.debug("REST request to get the  customer contacts by salesforceaccountID ");
        return customer_contacts;
    }

    /**
     * Get  /accounts/contacts/{tamdeskAccountId}-> Get contact using salesforceAccountId Id.
     */
    @RequestMapping(value = "/accounts/contacts/{tamdeskAccountId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Page<Customer_Contacts> getCustomerContacts(@RequestParam(value = "tamdeskAccountId") String tamdeskAccountId,Pageable pageable) {

        Page<Customer_Contacts> customer_contacts = customercontactService.getCustomerContacts(tamdeskAccountId,  pageable);
        log.debug("REST request to get the  customer contacts by tamdeskAccountId ");
        return customer_contacts;
    }

    /**
     * Get  /accounts/contacts/{tamdeskAccountId}-> Get contact using tamdeskAccountId Id.
     */
    @RequestMapping(value = "/accounts/ContactsByCustomerId/{tamdeskAccountId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Customer_Contacts> getCustomerContactsByCustomerId(@PathVariable Integer  tamdeskAccountId) {

        log.debug("REST request to get all customer contacts ");
        List<Customer_Contacts> customer_contacts = customercontactService.getCustomerContactsByCustomerId(tamdeskAccountId);

        return customer_contacts;
    }


}
