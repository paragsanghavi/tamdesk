package com.tamdesk.web.rest.customer;

import com.codahale.metrics.annotation.Timed;
import com.tamdesk.domain.customer.Customer;
import com.tamdesk.repository.customer.CustomerAccountTeamRepository;
import com.tamdesk.service.customer.CustomerService;
import com.tamdesk.web.rest.util.Count;
import com.tamdesk.domain.customer.Customer_Account_Team;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.*;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by psanghavi on 10/3/14.
 */

@RestController
@RequestMapping("/tamdesk/rest")
public class CustomerResource {

    private final Logger log = LoggerFactory.getLogger(CustomerResource.class);

    @Inject
    private CustomerService customerService;
    @Inject
    private CustomerAccountTeamRepository customercccountteamRepository;

    /**
     * POST  create a new note
     */
    @RequestMapping(value = "/accounts/create",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void createCustomer(customerDTO customerVo) {
        log.debug("**************Creating customer for account ID {}", customerVo.getName());
        customerService.create(customerVo.toCustomer());
    }


    /**
     * Get  /accounts/{customerId}-> Get account using customer Id.
     */
    @RequestMapping(value = "/accountsbycustid/{customerId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Customer getCustomerByID(@PathVariable Integer customerId) {

        Customer customer = customerService.getCustomerByCustomerId(customerId) ;
        log.debug("REST request to get the customer {}", customer.getName());
        return customer;
    }

    /**
     * Get  /accounts/{customerId}-> Get account using customer Id.
     */
    @RequestMapping(value = "/accounts/salesforce/{salesforceId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Customer getCustomerBySalesforceID(@PathParam("salesforceId") String salesforceId) {

        Customer customer = customerService.getCustomerBySalesForceId(salesforceId) ;
        log.debug("REST request to get the customer {}", customer.getName());
        return customer;
    }

    /**
     * Get  /accounts/top10Revenue/
     */
    @RequestMapping(value = "/accounts/top10Revenue",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Customer> top10CustomerByRevenue() {

        log.debug("REST request to get the top 10 customer by revenue ");

        List<Customer> customers = customerService.top10CustomerByRevenue() ;
        return customers;
    }
    /**
     * Get  /accounts/top10AdoptionScore/-> Get top 10 account by adoption score
     */
    @RequestMapping(value = "/accounts/top10atRiskCustomer",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Customer> top10atRiskCustomer() {

        log.debug("REST request to get the top 10 customer by revenue ");

        List<Customer> customers = customerService.top10CustomerByRiskScore() ;
        return customers;
    }
    /**
     * Get  /accounts/top10AdoptionScore/-> Get top 10 account by adoption score
     */
    @RequestMapping(value = "/accounts/top10AdoptionScore",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Customer> top10CustomerByAdoptionScore() {

        log.debug("REST request to get the top 10 customer by revenue ");

        List<Customer> customers = customerService.top10CustomerByAdoptionScore() ;
        return customers;
    }

    /**
     * Get  /accounts all the accounts by pages
     */
    @RequestMapping(value = "/accounts",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed

    public Page<Customer> getCustomers(Pageable pageable) {
            Page<Customer> customers = customerService.getCustomers(pageable);
            log.debug("REST request to get the top 10 customer by revenue ");
            return customers;
    }

    /**
     * Get  /accounts all the accounts by pages
     */
    @RequestMapping(value = "/getallCustomers",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed

    public List<Customer> getallCustomers( ) {
        List <Customer> customers = customerService.getallCustomers();
        log.debug("REST request to get all customers ");
        return customers;
    }
    /**
     * Get  /accounts all the accounts by pages
     */
    @RequestMapping(value = "/accounts/count",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed

    public Count getCustomerCount() {
        Long count =  customerService.getCount();
        log.debug("REST request to get the total customer count ");

        if(count != null){
            return new Count(count);
        }
        return new Count(0L);
    }

    /**
     * Get  /account team information
     */
    @RequestMapping(value = "/accounts/account/team/{customerId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed

    public Customer_Account_Team getAccountTeam(@PathVariable Integer customerId) {
        log.debug("REST request to get the customer team information for customer id **** {} ",customerId );
        Customer_Account_Team customer_account_team =  customercccountteamRepository.getAccountTeam(customerId);
        log.debug("REST request to get the customer team {} ",customer_account_team.getCustomer_name() );

        return   customer_account_team;
    }
}
