package com.tamdesk.web.rest.customer;

import com.codahale.metrics.annotation.Timed;
import com.tamdesk.domain.customer.Customer_Contacts;
import com.tamdesk.domain.customer.Customer_Opportunity;
import com.tamdesk.service.customer.CustomerOpportunityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by psanghavi on 10/5/14.
 */


@RestController
@RequestMapping("/tamdesk/rest")
public class CustomerOpportunityResource {

    private final Logger log = LoggerFactory.getLogger(CustomerOpportunityResource.class);

    @Inject
    private  CustomerOpportunityService customeropportunityService;

    /**
     * Get  /accounts/opportunities/{oppId}-> Get contact using contact Id.
     */
    @RequestMapping(value = "/accounts/opportunity/{oppId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Customer_Opportunity getOppByID(@RequestParam(value = "oppId") String oppId) {

        Customer_Opportunity customer_opportunity = customeropportunityService.getOpportunityById(oppId) ;
        log.debug("REST request to get the customer opportunity {}", customer_opportunity.getOpportunity_name());
        return customer_opportunity;
    }

    /**
     * Get  /accounts/contacts/{salesforceAccountId}-> Get contact using salesforceAccountId Id.
     */
    @RequestMapping(value = "/accounts/opportunitiesBySalesForceId/{salesforceAccountId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Page<Customer_Opportunity> getCustomerBySalesForceId(@RequestParam(value = "salesforceAccountId") String salesforceAccountId,Pageable pageable) {

        Page<Customer_Opportunity> customer_opportunities = customeropportunityService.getOpportunityBySalesForceId (salesforceAccountId,  pageable);
        log.debug("REST request to get the  customer opportunites by salesforceaccountID ");
        return customer_opportunities;
    }

    /**
     * Get  /accounts/contacts/{tamdeskAccountId}-> Get contact using salesforceAccountId Id.
     */
    @RequestMapping(value = "/accounts/opportunities/pageable/{tamdeskAccountId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Page<Customer_Opportunity> getCustomerContactsbyPage(@PathVariable Integer tamdeskAccountId,Pageable pageable) {

        Page<Customer_Opportunity> customer_opportunities = customeropportunityService.getOpportunities(tamdeskAccountId,pageable) ;
        log.debug("REST request to get the  customer opportunites by tamdeskAccountId ");
        return customer_opportunities;
    }

    /**
     * Get  /accounts/contacts/{tamdeskAccountId}-> Get contact using salesforceAccountId Id.
     */
    @RequestMapping(value = "/accounts/opportunities/{tamdeskAccountId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Customer_Opportunity> getCustomerContacts(@PathVariable Integer tamdeskAccountId ) {

        List<Customer_Opportunity> customer_opportunities = customeropportunityService.getOpportunities(tamdeskAccountId) ;
        log.debug("REST request to get the  customer opportunites by tamdeskAccountId ");
        return customer_opportunities;
    }
}
