package com.tamdesk.web.rest.customer;

import com.tamdesk.domain.customer.Customer;
import com.tamdesk.domain.customer.DeploymentDetail;
import com.tamdesk.domain.customer.DeploymentStatus;
import com.tamdesk.domain.customer.EnvironmentDetail;
import com.tamdesk.domain.customer.SaaSEnvironment;
import com.tamdesk.domain.customer.Customer_Region;
import org.hibernate.annotations.*;
import org.joda.time.LocalDateTime;


import java.util.List;

/**
 * Created by psanghavi on 10/3/14.
 */
public class customerDTO {



    private String Id;
    private String Name;

    private String salesForceId;

    private String zendeskId;
    private String Total_Account_Revenue;

    private Boolean isSaas;
    private Boolean isDeleted;


    private Customer_Region region;
    private String saasURL;

    private String customerPriority ;
    private String licenseType;

    //General
    private String Type;
    private String premiumLevel;
    private String AnnualRevenue;

    //Customer Information - Billing address

    private String BillingStreet;
    private String BillingCity ;
    private String BillingState;
    private String BillingPostalCode;
    private String BillingCountry ;
    private String BillingLatitude ;
    private String BillingLongitude;

    //Team Information
    private String salesAccountMgr;
    private String salesEngineer;
    private String Technical_Account_Manager;
    private String Secondary_Technical_Account_Manager;
    private String Customer_Relationship_manager;
    private String Technical_Architect;


    private LocalDateTime Date_TAM_Assigned_to_Account;
    private LocalDateTime Date_Architect_was_assigned_to_Account;
    private LocalDateTime Date_Secondary_TAM_was_assigned_to_Account;
    private LocalDateTime Date_CRM_was_assigned_to_Account;


    private String ExecutiveSponsor;
    private String EBChampion;
    private String TechChampion;


    //Customer Statistics
    private String sentiment;
    private Double adoptionScore;
    private Double riskScore;
    private Double Customer_Satisfaction_Score;
    private Double NPS_Score;
    private String customer_status;


    private DeploymentStatus deploymentStatus;
    private LocalDateTime  Last_Customer_Outreach;
    private LocalDateTime  Last_Date_of_Customer_Response;
    private LocalDateTime  New_Customer_Acquisition_Date;

    //customer environment details

    private List<EnvironmentDetail> environments;
    private List<DeploymentDetail> deployments;
    private List<SaaSEnvironment> saaSEnvironment;


    //Customer Documents

    private String GoogleDriveURL;
    private String SalesForceURL;


    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSalesForceId() {
        return salesForceId;
    }

    public void setSalesForceId(String salesForceId) {
        this.salesForceId = salesForceId;
    }

    public String getZendeskId() {
        return zendeskId;
    }

    public void setZendeskId(String zendeskId) {
        this.zendeskId = zendeskId;
    }

    public String getTotal_Account_Revenue() {
        return Total_Account_Revenue;
    }

    public void setTotal_Account_Revenue(String total_Account_Revenue) {
        Total_Account_Revenue = total_Account_Revenue;
    }

    public Boolean getIsSaas() {
        return isSaas;
    }

    public void setIsSaas(Boolean isSaas) {
        this.isSaas = isSaas;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Customer_Region getRegion() {
        return region;
    }

    public void setRegion(Customer_Region region) {
        this.region = region;
    }

    public String getSaasURL() {
        return saasURL;
    }

    public void setSaasURL(String saasURL) {
        this.saasURL = saasURL;
    }

    public String getCustomerPriority() {
        return customerPriority;
    }

    public void setCustomerPriority(String customerPriority) {
        this.customerPriority = customerPriority;
    }

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getPremiumLevel() {
        return premiumLevel;
    }

    public void setPremiumLevel(String premiumLevel) {
        this.premiumLevel = premiumLevel;
    }

    public String getAnnualRevenue() {
        return AnnualRevenue;
    }

    public void setAnnualRevenue(String annualRevenue) {
        AnnualRevenue = annualRevenue;
    }

    public String getBillingStreet() {
        return BillingStreet;
    }

    public void setBillingStreet(String billingStreet) {
        BillingStreet = billingStreet;
    }

    public String getBillingCity() {
        return BillingCity;
    }

    public void setBillingCity(String billingCity) {
        BillingCity = billingCity;
    }

    public String getBillingState() {
        return BillingState;
    }

    public void setBillingState(String billingState) {
        BillingState = billingState;
    }

    public String getBillingPostalCode() {
        return BillingPostalCode;
    }

    public void setBillingPostalCode(String billingPostalCode) {
        BillingPostalCode = billingPostalCode;
    }

    public String getBillingCountry() {
        return BillingCountry;
    }

    public void setBillingCountry(String billingCountry) {
        BillingCountry = billingCountry;
    }

    public String getBillingLatitude() {
        return BillingLatitude;
    }

    public void setBillingLatitude(String billingLatitude) {
        BillingLatitude = billingLatitude;
    }

    public String getBillingLongitude() {
        return BillingLongitude;
    }

    public void setBillingLongitude(String billingLongitude) {
        BillingLongitude = billingLongitude;
    }

    public String getSalesAccountMgr() {
        return salesAccountMgr;
    }

    public void setSalesAccountMgr(String salesAccountMgr) {
        this.salesAccountMgr = salesAccountMgr;
    }

    public String getSalesEngineer() {
        return salesEngineer;
    }

    public void setSalesEngineer(String salesEngineer) {
        this.salesEngineer = salesEngineer;
    }

    public String getTechnical_Account_Manager() {
        return Technical_Account_Manager;
    }

    public void setTechnical_Account_Manager(String technical_Account_Manager) {
        Technical_Account_Manager = technical_Account_Manager;
    }

    public String getSecondary_Technical_Account_Manager() {
        return Secondary_Technical_Account_Manager;
    }

    public void setSecondary_Technical_Account_Manager(String secondary_Technical_Account_Manager) {
        Secondary_Technical_Account_Manager = secondary_Technical_Account_Manager;
    }

    public String getCustomer_Relationship_manager() {
        return Customer_Relationship_manager;
    }

    public void setCustomer_Relationship_manager(String customer_Relationship_manager) {
        Customer_Relationship_manager = customer_Relationship_manager;
    }

    public String getTechnical_Architect() {
        return Technical_Architect;
    }

    public void setTechnical_Architect(String technical_Architect) {
        Technical_Architect = technical_Architect;
    }

    public LocalDateTime getDate_TAM_Assigned_to_Account() {
        return Date_TAM_Assigned_to_Account;
    }

    public void setDate_TAM_Assigned_to_Account(LocalDateTime date_TAM_Assigned_to_Account) {
        Date_TAM_Assigned_to_Account = date_TAM_Assigned_to_Account;
    }

    public LocalDateTime getDate_Architect_was_assigned_to_Account() {
        return Date_Architect_was_assigned_to_Account;
    }

    public void setDate_Architect_was_assigned_to_Account(LocalDateTime date_Architect_was_assigned_to_Account) {
        Date_Architect_was_assigned_to_Account = date_Architect_was_assigned_to_Account;
    }

    public LocalDateTime getDate_Secondary_TAM_was_assigned_to_Account() {
        return Date_Secondary_TAM_was_assigned_to_Account;
    }

    public void setDate_Secondary_TAM_was_assigned_to_Account(LocalDateTime date_Secondary_TAM_was_assigned_to_Account) {
        Date_Secondary_TAM_was_assigned_to_Account = date_Secondary_TAM_was_assigned_to_Account;
    }

    public LocalDateTime getDate_CRM_was_assigned_to_Account() {
        return Date_CRM_was_assigned_to_Account;
    }

    public void setDate_CRM_was_assigned_to_Account(LocalDateTime date_CRM_was_assigned_to_Account) {
        Date_CRM_was_assigned_to_Account = date_CRM_was_assigned_to_Account;
    }

    public String getExecutiveSponsor() {
        return ExecutiveSponsor;
    }

    public void setExecutiveSponsor(String executiveSponsor) {
        ExecutiveSponsor = executiveSponsor;
    }

    public String getEBChampion() {
        return EBChampion;
    }

    public void setEBChampion(String EBChampion) {
        this.EBChampion = EBChampion;
    }

    public String getTechChampion() {
        return TechChampion;
    }

    public void setTechChampion(String techChampion) {
        TechChampion = techChampion;
    }

    public String getSentiment() {
        return sentiment;
    }

    public void setSentiment(String sentiment) {
        this.sentiment = sentiment;
    }

    public Double getAdoptionScore() {
        return adoptionScore;
    }

    public void setAdoptionScore(Double adoptionScore) {
        this.adoptionScore = adoptionScore;
    }

    public Double getRiskScore() {
        return riskScore;
    }

    public void setRiskScore(Double riskScore) {
        this.riskScore = riskScore;
    }

    public Double getCustomer_Satisfaction_Score() {
        return Customer_Satisfaction_Score;
    }

    public void setCustomer_Satisfaction_Score(Double customer_Satisfaction_Score) {
        Customer_Satisfaction_Score = customer_Satisfaction_Score;
    }

    public Double getNPS_Score() {
        return NPS_Score;
    }

    public void setNPS_Score(Double NPS_Score) {
        this.NPS_Score = NPS_Score;
    }

    public String getCustomer_status() {
        return customer_status;
    }

    public void setCustomer_status(String customer_status) {
        this.customer_status = customer_status;
    }

    public DeploymentStatus getDeploymentStatus() {
        return deploymentStatus;
    }

    public void setDeploymentStatus(DeploymentStatus deploymentStatus) {
        this.deploymentStatus = deploymentStatus;
    }

    public LocalDateTime getLast_Customer_Outreach() {
        return Last_Customer_Outreach;
    }

    public void setLast_Customer_Outreach(LocalDateTime last_Customer_Outreach) {
        Last_Customer_Outreach = last_Customer_Outreach;
    }

    public LocalDateTime getLast_Date_of_Customer_Response() {
        return Last_Date_of_Customer_Response;
    }

    public void setLast_Date_of_Customer_Response(LocalDateTime last_Date_of_Customer_Response) {
        Last_Date_of_Customer_Response = last_Date_of_Customer_Response;
    }

    public LocalDateTime getNew_Customer_Acquisition_Date() {
        return New_Customer_Acquisition_Date;
    }

    public void setNew_Customer_Acquisition_Date(LocalDateTime new_Customer_Acquisition_Date) {
        New_Customer_Acquisition_Date = new_Customer_Acquisition_Date;
    }

    public List<EnvironmentDetail> getEnvironments() {
        return environments;
    }

    public void setEnvironments(List<EnvironmentDetail> environments) {
        this.environments = environments;
    }

    public List<DeploymentDetail> getDeployments() {
        return deployments;
    }

    public void setDeployments(List<DeploymentDetail> deployments) {
        this.deployments = deployments;
    }

    public List<SaaSEnvironment> getSaaSEnvironment() {
        return saaSEnvironment;
    }

    public void setSaaSEnvironment(List<SaaSEnvironment> saaSEnvironment) {
        this.saaSEnvironment = saaSEnvironment;
    }

    public String getGoogleDriveURL() {
        return GoogleDriveURL;
    }

    public void setGoogleDriveURL(String googleDriveURL) {
        GoogleDriveURL = googleDriveURL;
    }

    public String getSalesForceURL() {
        return SalesForceURL;
    }

    public void setSalesForceURL(String salesForceURL) {
        SalesForceURL = salesForceURL;
    }



    public Customer toCustomer(){
        Customer customer = new Customer();

        customer.setName(this.getName());
        customer.setAdoptionScore(this.getAdoptionScore());
        customer.setCustomer_Relationship_manager(this.getCustomer_Relationship_manager());
        customer.setTechnical_Account_Manager(this.getTechnical_Account_Manager());
        customer.setCustomer_Satisfaction_Score(this.getCustomer_Satisfaction_Score());
        customer.setCustomer_status(this.getCustomer_status());
        customer.setSalesAccountMgr(this.getSalesAccountMgr());
        customer.setCustomerPriority(this.getCustomerPriority());

        customer.setSalesEngineer(this.getSalesEngineer());
        customer.setSecondary_Technical_Account_Manager(this.getSecondary_Technical_Account_Manager());
        customer.setTechChampion(this.getTechChampion());
        customer.setGoogleDriveURL(this.getGoogleDriveURL());
        customer.setExecutiveSponsor(this.getExecutiveSponsor());
        customer.setCustomer_Satisfaction_Score(this.getCustomer_Satisfaction_Score());
        customer.setEBChampion(this.getEBChampion());
        customer.setTechnical_Architect(this.getTechnical_Architect());
        customer.setIsSaas(this.getIsSaas());
        customer.setNPS_Score(this.getNPS_Score());
        customer.setDeploymentStatus(this.getDeploymentStatus());
        customer.setLicenseType(this.getLicenseType());
        customer.setEnvironments(this.getEnvironments());
        customer.setSaaSEnvironment(this.getSaaSEnvironment());

        customer.setNew_Customer_Acquisition_Date(this.getNew_Customer_Acquisition_Date());
        customer.setDate_TAM_Assigned_to_Account(this.getDate_TAM_Assigned_to_Account());
        customer.setDate_Secondary_TAM_was_assigned_to_Account(this.getDate_Secondary_TAM_was_assigned_to_Account());
        customer.setDate_CRM_was_assigned_to_Account(this.getDate_CRM_was_assigned_to_Account());
        customer.setDate_Architect_was_assigned_to_Account(this.getDate_Architect_was_assigned_to_Account());

        customer.setBillingStreet(this.getBillingStreet());
        customer.setBillingCity(this.getBillingCity());
        customer.setBillingCountry(this.getBillingCountry());
        customer.setBillingPostalCode(this.getBillingPostalCode());
        customer.setBillingState(this.getBillingState());
        customer.setCustomer_region(this.region);
        customer.setSaasURL(this.saasURL);
        customer.setGoogleDriveURL(this.getGoogleDriveURL());

        return customer;


    }

    public customerDTO(String id, String name, String salesForceId, String zendeskId, String total_Account_Revenue,
                       Boolean isSaas, Boolean isDeleted,
                       Customer_Region region, String saasURL, String customerPriority, String licenseType,
                       String type, String premiumLevel, String annualRevenue, String billingStreet,
                       String billingCity, String billingState, String billingPostalCode, String billingCountry, String billingLatitude,
                       String billingLongitude, String salesAccountMgr, String salesEngineer, String technical_Account_Manager,
                       String secondary_Technical_Account_Manager, String customer_Relationship_manager, String technical_Architect,
                       LocalDateTime date_TAM_Assigned_to_Account, LocalDateTime date_Architect_was_assigned_to_Account,
                       LocalDateTime date_Secondary_TAM_was_assigned_to_Account, LocalDateTime date_CRM_was_assigned_to_Account,
                       String executiveSponsor, String EBChampion, String techChampion, String sentiment, Double adoptionScore,
                       Double riskScore, Double customer_Satisfaction_Score, Double NPS_Score, String customer_status,
                       DeploymentStatus deploymentStatus, LocalDateTime last_Customer_Outreach, LocalDateTime last_Date_of_Customer_Response,
                       LocalDateTime new_Customer_Acquisition_Date, List<EnvironmentDetail> environments, List<DeploymentDetail> deployments,
                       List<SaaSEnvironment> saaSEnvironment, String googleDriveURL, String salesForceURL) {
        Id = id;
        Name = name;
        this.salesForceId = salesForceId;
        this.zendeskId = zendeskId;
        Total_Account_Revenue = total_Account_Revenue;
        this.isSaas = isSaas;
        this.isDeleted = isDeleted;
        this.region = region;
        this.saasURL = saasURL;
        this.customerPriority = customerPriority;
        this.licenseType = licenseType;
        Type = type;
        this.premiumLevel = premiumLevel;
        AnnualRevenue = annualRevenue;
        BillingStreet = billingStreet;
        BillingCity = billingCity;
        BillingState = billingState;
        BillingPostalCode = billingPostalCode;
        BillingCountry = billingCountry;
        BillingLatitude = billingLatitude;
        BillingLongitude = billingLongitude;
        this.salesAccountMgr = salesAccountMgr;
        this.salesEngineer = salesEngineer;
        Technical_Account_Manager = technical_Account_Manager;
        Secondary_Technical_Account_Manager = secondary_Technical_Account_Manager;
        Customer_Relationship_manager = customer_Relationship_manager;
        Technical_Architect = technical_Architect;
        Date_TAM_Assigned_to_Account = date_TAM_Assigned_to_Account;
        Date_Architect_was_assigned_to_Account = date_Architect_was_assigned_to_Account;
        Date_Secondary_TAM_was_assigned_to_Account = date_Secondary_TAM_was_assigned_to_Account;
        Date_CRM_was_assigned_to_Account = date_CRM_was_assigned_to_Account;
        ExecutiveSponsor = executiveSponsor;
        this.EBChampion = EBChampion;
        TechChampion = techChampion;
        this.sentiment = sentiment;
        this.adoptionScore = adoptionScore;
        this.riskScore = riskScore;
        Customer_Satisfaction_Score = customer_Satisfaction_Score;
        this.NPS_Score = NPS_Score;
        this.customer_status = customer_status;
        this.deploymentStatus = deploymentStatus;
        Last_Customer_Outreach = last_Customer_Outreach;
        Last_Date_of_Customer_Response = last_Date_of_Customer_Response;
        New_Customer_Acquisition_Date = new_Customer_Acquisition_Date;
        this.environments = environments;
        this.deployments = deployments;
        this.saaSEnvironment = saaSEnvironment;
        GoogleDriveURL = googleDriveURL;
        SalesForceURL = salesForceURL;
    }
}
