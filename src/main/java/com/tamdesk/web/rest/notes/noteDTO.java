package com.tamdesk.web.rest.notes;

import com.tamdesk.domain.notes.*;
/**
 * Created by psanghavi on 9/24/14.
 */
public class noteDTO {

    private String id;
    private String title;
    private String content;
    private String tags;
    private String account_id;
    private String created_by;
    private String Created_at;
    private String last_modified_by;
    private String last_modified_at;
    private Boolean isCustomerFacingNote;

    private NoteType notetype;

    public noteDTO(String id, String title, String content, String tags, String account_id, String created_by, String created_at, String last_modified_by, String last_modified_at, Boolean isCustomerFacingNote, NoteType notetype) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.tags = tags;
        this.account_id = account_id;
        this.created_by = created_by;
        Created_at = created_at;
        this.last_modified_by = last_modified_by;
        this.last_modified_at = last_modified_at;
        this.isCustomerFacingNote = isCustomerFacingNote;
        this.notetype = notetype;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getCreated_at() {
        return Created_at;
    }

    public void setCreated_at(String created_at) {
        Created_at = created_at;
    }

    public String getLast_modified_by() {
        return last_modified_by;
    }

    public void setLast_modified_by(String last_modified_by) {
        this.last_modified_by = last_modified_by;
    }

    public String getLast_modified_at() {
        return last_modified_at;
    }

    public void setLast_modified_at(String last_modified_at) {
        this.last_modified_at = last_modified_at;
    }

    public Boolean getIsCustomerFacingNote() {
        return isCustomerFacingNote;
    }

    public void setIsCustomerFacingNote(Boolean isCustomerFacingNote) {
        this.isCustomerFacingNote = isCustomerFacingNote;
    }

    public NoteType getNotetype() {
        return notetype;
    }

    public void setNotetype(NoteType notetype) {
        this.notetype = notetype;
    }

    public static Note CreateEmptyNote(){
        return new Note();
    }

    public Note toNote(){
        Note note = new Note();
        note.setTitle(this.getTitle());
        note.setContent(this.getContent());
        note.setNotetype(this.getNotetype());
        note.setIsCustomerFacingNote(this.getIsCustomerFacingNote());

        note.setTags(this.getTags());
        note.setCreatedByUser(this.getCreated_by());
        note.setCreatedat(this.getCreated_at());
        note.setLastmodifiedat(this.getLast_modified_at());
        note.setLastmodifiedbyUser(this.getLast_modified_by());

        note.getCustomer().setCustomer_id(Integer.parseInt(this.getAccount_id()));
        return note;

    }
}
