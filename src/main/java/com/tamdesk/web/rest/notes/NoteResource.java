package com.tamdesk.web.rest.notes;

/**
 * Created by psanghavi on 9/24/14.
 */

import com.codahale.metrics.annotation.Timed;
import com.tamdesk.repository.notes.NoteRepository;

import com.tamdesk.domain.notes.Note;
import com.tamdesk.domain.notes.Notes;
import com.tamdesk.service.notes.NoteService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.context.IWebContext;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.context.SpringWebContext;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;


/**
 * REST controller for managing the account's notes .
 */
@RestController
@RequestMapping("/tamdesk/rest")

public class NoteResource {

    private final Logger log = LoggerFactory.getLogger(NoteResource.class);

    @Inject
    private NoteService noteService;

    /**
     * POST  create a new note
     */
    @RequestMapping(value = "/accounts/{accountId}/notes/create",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void createNote(noteDTO noteVo) {
       log.debug("**************Creating note for account ID {}", noteVo.getAccount_id());
       noteService.create(noteVo.toNote());
    }

    /**
     * Get  /accounts/{accountId}/notes -> Get all notes for the account.
     */
    @RequestMapping(value = "/accounts/{accountId}/notes",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Notes getNotes(@RequestParam(value = "accountId") String accountId) {

        log.debug("REST request to get the all the notes for account {}", accountId);
        return noteService.getNotes(accountId);

    }

    /**
     * Get  /accounts/{accountId}/count -> Get the total number of notes for the account.
     */
    @RequestMapping(value = "/accounts/{accountId}/notes/count",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Long getNotesCount(@RequestParam(value = "accountId") String accountId) {
        Long count = noteService.getNotesCount(accountId);
        if (count != null) {
            return count;
        }

        log.debug("REST request to get the total number of notes for account {}", accountId, count);
        return 0L;
    }

    /**
     * Get  /accounts/{accountId}/noteid -> Get the note for the account based on note id.
     */
    @RequestMapping(value = "/accounts/{accountId}/notes/{noteid}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed

    public Note getNote(@RequestParam(value = "noteid") String noteId) {

        log.debug("REST request to get the details of the note  {}", noteId);
        return noteService.getNote(noteId);
    }

    /**
     * Get  /accounts/{accountId}/notes/new -> Create an empty note object.
     */
    @RequestMapping(value = "/accounts/{accountId}/notes/new",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public Note getNote() {
        return noteDTO.CreateEmptyNote();
    }

    /**
     * Get  /accounts/{accountId}/delete/noteid -> Delete the note for the account based on note id.
     */
    @RequestMapping(value = "/accounts/{accountId}/note/delete/{noteid}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed

    public void deleteNote(@RequestParam(value = "accountId") String accountId, @RequestParam(value = "noteid") String noteId) {
        noteService.delete(accountId, noteId);
    }
}

