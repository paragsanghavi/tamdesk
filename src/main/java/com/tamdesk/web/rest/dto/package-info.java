/**
 * Data Access Objects used by Spring MVC REST controllers.
 */
package com.tamdesk.web.rest.dto;
