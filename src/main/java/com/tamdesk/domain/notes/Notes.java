package com.tamdesk.domain.notes;


import java.util.ArrayList;
import java.util.List;
/**
 * Created by psanghavi on 9/23/14.
 */
public class Notes {

    private List<Note> notes = new ArrayList<Note>();

    public List<Note> getNotes() {
        return notes;
    }
    public Notes(){
        //Default constructor
    }
    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }
}
