package com.tamdesk.domain.notes;

/**
 * Created by psanghavi on 9/23/14.
 */


 import com.fasterxml.jackson.annotation.JsonIgnore;
 import com.tamdesk.domain.AbstractAuditingEntity;
 import com.tamdesk.domain.customer.Customer;
 import org.hibernate.annotations.Cache;
 import org.hibernate.annotations.CacheConcurrencyStrategy;

 import javax.persistence.*;
 import javax.validation.constraints.NotNull;
 import javax.validation.constraints.Size;
 import java.io.Serializable;


@Entity
@Table(name = "tamdesk_notes")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)

public class Note extends AbstractAuditingEntity implements Serializable {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "note_id")
    private Integer note_id;

    @Size(min = 0, max = 1024)
    @Column(name = "title")
    private String title;

    @Size(min = 0, max = 8192)
    @Column(name = "content")
    private String content;

    @Size(min = 0, max = 1024)
    @Column(name = "tags")
    private String tags;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="customer_id")
    private Customer customer;

    @Size(min = 0, max = 50)
    @Column(name = "created_by_user")
    private String createdByUser;

    @Size(min = 0, max = 50)
    @Column(name = "Created_at")
    private String Createdat;

    @Size(min = 0, max = 50)
    @Column(name = "last_modified_by_user")
    private String lastmodifiedbyUser;

    @Size(min = 0, max = 50)
    @Column(name = "last_modified_at")
    private String lastmodifiedat;

    @Size(min = 0, max = 50)
    @Column(name = "isCustomerFacingNote")
    private Boolean isCustomerFacingNote;

    @Size(min = 0, max = 50)
    @Column(name = "notetype")
    private NoteType notetype;

    private Boolean isStickyNote;



    public Note() {
    }

    public Integer getNote_id() {
        return note_id;
    }

    public void setNote_id(Integer note_id) {
        this.note_id = note_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getCreatedByUser() {
        return createdByUser;
    }

    public void setCreatedByUser(String createdByUser) {
        this.createdByUser = createdByUser;
    }

    public String getCreatedat() {
        return Createdat;
    }

    public void setCreatedat(String createdat) {
        Createdat = createdat;
    }

    public String getLastmodifiedbyUser() {
        return lastmodifiedbyUser;
    }

    public void setLastmodifiedbyUser(String lastmodifiedbyUser) {
        this.lastmodifiedbyUser = lastmodifiedbyUser;
    }

    public String getLastmodifiedat() {
        return lastmodifiedat;
    }

    public void setLastmodifiedat(String lastmodifiedat) {
        this.lastmodifiedat = lastmodifiedat;
    }

    public Boolean getIsCustomerFacingNote() {
        return isCustomerFacingNote;
    }

    public void setIsCustomerFacingNote(Boolean isCustomerFacingNote) {
        this.isCustomerFacingNote = isCustomerFacingNote;
    }

    public NoteType getNotetype() {
        return notetype;
    }

    public void setNotetype(NoteType notetype) {
        this.notetype = notetype;
    }

    public Boolean getIsStickyNote() {
        return isStickyNote;
    }

    public void setIsStickyNote(Boolean isStickyNote) {
        this.isStickyNote = isStickyNote;
    }
}
