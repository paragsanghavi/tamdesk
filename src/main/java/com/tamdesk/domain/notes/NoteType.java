package com.tamdesk.domain.notes;

/**
 * Created by psanghavi on 9/23/14.
 */
public enum NoteType {
    general,
    yellow,
    red,
    escalation,
    enhancement,
    upsell,
    crosssell,
    Terms_and_Conditions_customer,
    Terms_and_Conditions_project,
    project_activity_note
}
