package com.tamdesk.domain.project;

import com.tamdesk.domain.notes.Note;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by psanghavi on 10/6/14.
 */

@Entity
@Table(name = "tamdesk_customer_projects_activity")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Project_Activity {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Integer activity_id;

    @ManyToOne
    @JoinColumn(name="project_id")
    private Project project;

    private String username;
    private Integer username_id;

    @Enumerated(EnumType.STRING)
    private Project_Activity_Type project_activity_type;

    private Integer project_activity_hours;

    @Enumerated(EnumType.STRING)
    private Project_Activity_Location project_activity_location;

    @Size(min = 0, max = 2048)
    private String activity_description;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime activity_start_date;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime activity_end_date;

    @Enumerated(EnumType.STRING)
    private Project_Activity_Status project_activity_status;

    public Integer getId() {
        return activity_id;
    }

    public void setId(Integer id) {
        this.activity_id = id;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Project_Activity_Type getProject_activity_type() {
        return project_activity_type;
    }

    public void setProject_activity_type(Project_Activity_Type project_activity_type) {
        this.project_activity_type = project_activity_type;
    }

    public String getActivity_description() {
        return activity_description;
    }

    public void setActivity_description(String activity_description) {
        this.activity_description = activity_description;
    }

    public LocalDateTime getActivity_start_date() {
        return activity_start_date;
    }

    public void setActivity_start_date(LocalDateTime activity_start_date) {
        this.activity_start_date = activity_start_date;
    }

    public LocalDateTime getActivity_end_date() {
        return activity_end_date;
    }

    public void setActivity_end_date(LocalDateTime activity_end_date) {
        this.activity_end_date = activity_end_date;
    }

    public Project_Activity_Status getProject_activity_status() {
        return project_activity_status;
    }

    public void setProject_activity_status(Project_Activity_Status project_activity_status) {
        this.project_activity_status = project_activity_status;
    }

    public Integer getActivity_id() {
        return activity_id;
    }

    public void setActivity_id(Integer activity_id) {
        this.activity_id = activity_id;
    }

    public Project_Activity_Location getProject_activity_location() {
        return project_activity_location;
    }

    public void setProject_activity_location(Project_Activity_Location project_activity_location) {
        this.project_activity_location = project_activity_location;
    }

    public Integer getProject_activity_hours() {
        return project_activity_hours;
    }

    public void setProject_activity_hours(Integer project_activity_hours) {
        this.project_activity_hours = project_activity_hours;
    }

    public Integer getUsername_id() {
        return username_id;
    }

    public void setUsername_id(Integer username_id) {
        this.username_id = username_id;
    }
}
