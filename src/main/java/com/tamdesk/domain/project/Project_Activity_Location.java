package com.tamdesk.domain.project;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Created by psanghavi on 10/6/14.
 */

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Project_Activity_Location {
    //ONSITE,REMOTE

    ONSITE(1, "ONSITE"), REMOTE(2, "REMOTE");

    private Integer id;
    private String name;

    Project_Activity_Location(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
