package com.tamdesk.domain.project;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by psanghavi on 10/6/14.
 */

@Entity
@Table(name = "tamdesk_customer_service_code")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Service_Codes {
   //We need to populate this class through code or through the GUI

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "service_code_id")
    private Integer service_code_id;

    private String   service_code_name;

    /*@OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="package_id")
    @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Service_Packages service_packages;    */

    public Integer getService_code_id() {
        return service_code_id;
    }

    public void setService_code_id(Integer service_code_id) {
        this.service_code_id = service_code_id;
    }

    public String getService_code_name() {
        return service_code_name;
    }

    public void setService_code_name(String service_code_name) {
        this.service_code_name = service_code_name;
    }


}
