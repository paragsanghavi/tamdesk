package com.tamdesk.domain.project;

/**
 * Created by psanghavi on 10/6/14.
 */
public enum ProjectType {
    NEW, EXPANSION, SUPPORT, RENEWAL, UPSELL
}
