package com.tamdesk.domain.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tamdesk.domain.customer.Customer;
import org.hibernate.annotations.*;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.persistence.CascadeType;
import java.util.List;

/**
 * Created by psanghavi on 9/28/14.
 */

@Entity
@Table(name = "tamdesk_customer_projects")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Project {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "project_id")
    private Integer id;


    @ManyToOne
    @JoinColumn(name="customer_id")
    private Customer customer;


    @NotNull
    private  Integer tamdesk_opportunity_id;    //tamdesk opportunity Id


    @Size(min = 0, max = 50)
    private String salesforceAccountId;


    @Size(min = 0, max = 50)
    private  String salesforce_opportunity_id;

    @OneToMany(cascade=CascadeType.ALL)
    @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<Services> services;

    @OneToMany(cascade=CascadeType.ALL)
    @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<Project_Activity> project_activities;


    private String project_name;

    private String description;

    private String projectcol;

    @Enumerated(EnumType.STRING)
    private Project_Status Status;

    @Enumerated(EnumType.STRING)
    private Project_Adoption_Status project_adoption_status;

    @Enumerated(EnumType.STRING)
    private ProjectType project_type;


    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime project_close_date;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime project_extension_date;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime start_date;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime  end_date;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime  transition_meeting_date;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime  kickoff_date;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime  architecture_start_date;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime  sixty_day_EBMeeting_date;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime  one_twenty_day_EBMeeting_date;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime  one_eighty_day_EBMeeting_date;

    private String  project_type_id;
    private String  project_status_id;
    private Double  project_adoption_score;



    //Project Documents
    @Size(min = 0, max = 2048)
    private String GoogleDriveURL;

    @Size(min = 0, max = 2048)
    private String SalesForceURL;

    private Boolean  IsNonStandardTermsandConditions;

    private Boolean  IsEBMeetingScheduled;
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProjectcol() {
        return projectcol;
    }

    public void setProjectcol(String projectcol) {
        this.projectcol = projectcol;
    }



    public LocalDateTime getProject_close_date() {
        return project_close_date;
    }

    public void setProject_close_date(LocalDateTime project_close_date) {
        this.project_close_date = project_close_date;
    }

    public LocalDateTime getProject_extension_date() {
        return project_extension_date;
    }

    public void setProject_extension_date(LocalDateTime project_extension_date) {
        this.project_extension_date = project_extension_date;
    }

    public LocalDateTime getStart_date() {
        return start_date;
    }

    public void setStart_date(LocalDateTime start_date) {
        this.start_date = start_date;
    }

    public LocalDateTime getEnd_date() {
        return end_date;
    }

    public void setEnd_date(LocalDateTime end_date) {
        this.end_date = end_date;
    }

    public LocalDateTime getTransition_meeting_date() {
        return transition_meeting_date;
    }

    public void setTransition_meeting_date(LocalDateTime transition_meeting_date) {
        this.transition_meeting_date = transition_meeting_date;
    }

    public LocalDateTime getKickoff_date() {
        return kickoff_date;
    }

    public void setKickoff_date(LocalDateTime kickoff_date) {
        this.kickoff_date = kickoff_date;
    }

    public LocalDateTime getArchitecture_start_date() {
        return architecture_start_date;
    }

    public void setArchitecture_start_date(LocalDateTime architecture_start_date) {
        this.architecture_start_date = architecture_start_date;
    }

    public String getProject_type_id() {
        return project_type_id;
    }

    public void setProject_type_id(String project_type_id) {
        this.project_type_id = project_type_id;
    }

    public String getProject_status_id() {
        return project_status_id;
    }

    public void setProject_status_id(String project_status_id) {
        this.project_status_id = project_status_id;
    }

    public Double getProject_adoption_score() {
        return project_adoption_score;
    }

    public void setProject_adoption_score(Double project_adoption_score) {
        this.project_adoption_score = project_adoption_score;
    }



    public String getGoogleDriveURL() {
        return GoogleDriveURL;
    }

    public void setGoogleDriveURL(String googleDriveURL) {
        GoogleDriveURL = googleDriveURL;
    }

    public String getSalesForceURL() {
        return SalesForceURL;
    }

    public void setSalesForceURL(String salesForceURL) {
        SalesForceURL = salesForceURL;
    }

    public Boolean getIsNonStandardTermsandConditions() {
        return IsNonStandardTermsandConditions;
    }

    public void setIsNonStandardTermsandConditions(Boolean isNonStandardTermsandConditions) {
        IsNonStandardTermsandConditions = isNonStandardTermsandConditions;
    }

    public LocalDateTime getSixty_day_EBMeeting_date() {
        return sixty_day_EBMeeting_date;
    }

    public void setSixty_day_EBMeeting_date(LocalDateTime sixty_day_EBMeeting_date) {
        this.sixty_day_EBMeeting_date = sixty_day_EBMeeting_date;
    }

    public LocalDateTime getOne_twenty_day_EBMeeting_date() {
        return one_twenty_day_EBMeeting_date;
    }

    public void setOne_twenty_day_EBMeeting_date(LocalDateTime one_twenty_day_EBMeeting_date) {
        this.one_twenty_day_EBMeeting_date = one_twenty_day_EBMeeting_date;
    }

    public LocalDateTime getOne_eighty_day_EBMeeting_date() {
        return one_eighty_day_EBMeeting_date;
    }

    public void setOne_eighty_day_EBMeeting_date(LocalDateTime one_eighty_day_EBMeeting_date) {
        this.one_eighty_day_EBMeeting_date = one_eighty_day_EBMeeting_date;
    }

    public Project_Status getStatus() {
        return Status;
    }

    public void setStatus(Project_Status status) {
        Status = status;
    }

    public Project_Adoption_Status getProject_adoption_status() {
        return project_adoption_status;
    }

    public void setProject_adoption_status(Project_Adoption_Status project_adoption_status) {
        this.project_adoption_status = project_adoption_status;
    }

    public ProjectType getProject_type() {
        return project_type;
    }

    public void setProject_type(ProjectType project_type) {
        this.project_type = project_type;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Services> getServices() {
        return services;
    }

    public void setServices(List<Services> services) {
        this.services = services;
    }

    public Integer getTamdesk_opportunity_id() {
        return tamdesk_opportunity_id;
    }

    public void setTamdesk_opportunity_id(Integer tamdesk_opportunity_id) {
        this.tamdesk_opportunity_id = tamdesk_opportunity_id;
    }

    public String getSalesforceAccountId() {
        return salesforceAccountId;
    }

    public void setSalesforceAccountId(String salesforceAccountId) {
        this.salesforceAccountId = salesforceAccountId;
    }

    public String getSalesforce_opportunity_id() {
        return salesforce_opportunity_id;
    }

    public void setSalesforce_opportunity_id(String salesforce_opportunity_id) {
        this.salesforce_opportunity_id = salesforce_opportunity_id;
    }

    public List<Project_Activity> getProject_activities() {
        return project_activities;
    }

    public void setProject_activities(List<Project_Activity> project_activities) {
        this.project_activities = project_activities;
    }

    public Boolean getIsEBMeetingScheduled() {
        return IsEBMeetingScheduled;
    }

    public void setIsEBMeetingScheduled(Boolean isEBMeetingScheduled) {
        IsEBMeetingScheduled = isEBMeetingScheduled;
    }
}
