package com.tamdesk.domain.project;

/**
 * Created by psanghavi on 10/6/14.
 */
public enum Project_Status {

    ACTIVE, CLOSED, PENDING_CLOSURE
}
