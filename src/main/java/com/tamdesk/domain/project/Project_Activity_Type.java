package com.tamdesk.domain.project;

import com.fasterxml.jackson.annotation.JsonFormat;
/**
 * Created by psanghavi on 10/6/14.
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)

public enum Project_Activity_Type {

    ENABLEMENT(1, "ENABLEMENT"), ARCHITECT(2, "ARCHITECT"), PRODUCT_WARRANTY(3, "PRODUCT WARRANTY"), SALES_GIVEAWAY(4, "SALES GIVEAWAY"), SERVICES_WARRANTY(5, "SERVICES WARRANTY"), TRAINING_CLASS(6, "TRAINING CLASS"), NON_PROJECT_COMMITMENT(7, "NON_PROJECT_COMMITMENT"), KICKOFF_CALL(8, "KICKOFF CALL"),;

    //ENABLEMENT, ARCHITECT,  PRODUCT_WARRANTY, SALES_GIVEAWAY, SERVICES_WARRANTY,  TRAINING_CLASS, NON_PROJECT_COMMITMENT,KICKOFF_CALL

    private Integer id;
    private String name;

    Project_Activity_Type(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
