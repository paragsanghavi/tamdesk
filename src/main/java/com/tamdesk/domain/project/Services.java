package com.tamdesk.domain.project;

import com.tamdesk.domain.customer.Customer;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by psanghavi on 10/6/14.
 */

@Entity
@Table(name = "tamdesk_customer_services")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Services {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "service_id")
    private Integer service_id;



    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="project_id")
    private Project project;


    @OneToOne(cascade=CascadeType.ALL)
    @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinColumn(name="package_id")
    private Service_Packages package_purchased;

    private Integer quantity_purchased;

    private String package_name;
    private String SKU;
    private Integer service_days;
    private Integer architect_days;
    private Integer service_warranty_days;
    private Integer product_warranty_days;
    private Integer sales_giveaway_days;
    private Integer public_training_class_seats;
    private Integer private_training_class;
    private Integer services_units;


    public Integer getService_id() {
        return service_id;
    }

    public void setService_id(Integer service_id) {
        this.service_id = service_id;
    }

    public Service_Packages getPackage_purchased() {
        return package_purchased;
    }

    public void setPackage_purchased(Service_Packages package_purchased) {
        this.package_purchased = package_purchased;
    }


    public Integer getQuantity_purchased() {
        return quantity_purchased;
    }

    public void setQuantity_purchased(Integer quantity_purchased) {
        this.quantity_purchased = quantity_purchased;
    }

    public Integer getService_days() {
        return service_days;
    }

    public void setService_days(Integer service_days) {
        this.service_days = service_days;
    }

    public Integer getArchitect_days() {
        return architect_days;
    }

    public void setArchitect_days(Integer architect_days) {
        this.architect_days = architect_days;
    }

    public Integer getService_warranty_days() {
        return service_warranty_days;
    }

    public void setService_warranty_days(Integer service_warranty_days) {
        this.service_warranty_days = service_warranty_days;
    }

    public Integer getProduct_warranty_days() {
        return product_warranty_days;
    }

    public void setProduct_warranty_days(Integer product_warranty_days) {
        this.product_warranty_days = product_warranty_days;
    }

    public Integer getSales_giveaway_days() {
        return sales_giveaway_days;
    }

    public void setSales_giveaway_days(Integer sales_giveaway_days) {
        this.sales_giveaway_days = sales_giveaway_days;
    }

    public Integer getPublic_training_class_seats() {
        return public_training_class_seats;
    }

    public void setPublic_training_class_seats(Integer public_training_class_seats) {
        this.public_training_class_seats = public_training_class_seats;
    }

    public Integer getPrivate_training_class() {
        return private_training_class;
    }

    public void setPrivate_training_class(Integer private_training_class) {
        this.private_training_class = private_training_class;
    }

    public Integer getServices_units() {
        return services_units;
    }

    public void setServices_units(Integer services_units) {
        this.services_units = services_units;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getPackage_name() {
        return package_name;
    }

    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }

    public String getSKU() {
        return SKU;
    }

    public void setSKU(String SKU) {
        this.SKU = SKU;
    }
}
