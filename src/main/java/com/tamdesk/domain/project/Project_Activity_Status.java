package com.tamdesk.domain.project;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Created by psanghavi on 10/6/14.
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Project_Activity_Status {
    //SCHEDULED,TENTATIVE,COMPLETED

    SCHEDULED(1, "SCHEDULED"), TENTATIVE(2, "TENTATIVE"),COMPLETED(3, "COMPLETED");
    private Integer id;
    private String name;

    Project_Activity_Status(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
