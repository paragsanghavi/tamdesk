package com.tamdesk.domain.project;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by psanghavi on 10/6/14.
 * //We need to populate this class through code or through the GUI
 */

@Entity
@Table(name = "tamdesk_customer_service_packages")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)

public class Service_Packages {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "package_id")
    private Integer package_id;

    private String package_name;
    private String SKU;
    private String description;
    private Integer service_days;
    private Integer architect_days;
    private Integer service_warranty_days;
    private Integer product_warranty_days;
    private Integer sales_giveaway_days;
    private Integer public_training_class_seats;
    private Integer private_training_class;
    private Integer services_units;



    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="service_code_id")
    @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Service_Codes service_codes;

    public Integer getPackage_id() {
        return package_id;
    }

    public void setPackage_id(Integer package_id) {
        this.package_id = package_id;
    }

    public String getPackage_name() {
        return package_name;
    }

    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }

    public String getSKU() {
        return SKU;
    }

    public void setSKU(String SKU) {
        this.SKU = SKU;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    public Integer getService_days() {
        return service_days;
    }

    public void setService_days(Integer service_days) {
        this.service_days = service_days;
    }

    public Integer getArchitect_days() {
        return architect_days;
    }

    public void setArchitect_days(Integer architect_days) {
        this.architect_days = architect_days;
    }

    public Integer getService_warranty_days() {
        return service_warranty_days;
    }

    public void setService_warranty_days(Integer service_warranty_days) {
        this.service_warranty_days = service_warranty_days;
    }

    public Integer getProduct_warranty_days() {
        return product_warranty_days;
    }

    public void setProduct_warranty_days(Integer product_warranty_days) {
        this.product_warranty_days = product_warranty_days;
    }

    public Integer getSales_giveaway_days() {
        return sales_giveaway_days;
    }

    public void setSales_giveaway_days(Integer sales_giveaway_days) {
        this.sales_giveaway_days = sales_giveaway_days;
    }

    public Integer getPublic_training_class_seats() {
        return public_training_class_seats;
    }

    public void setPublic_training_class_seats(Integer public_training_class_seats) {
        this.public_training_class_seats = public_training_class_seats;
    }

    public Integer getPrivate_training_class() {
        return private_training_class;
    }

    public void setPrivate_training_class(Integer private_training_class) {
        this.private_training_class = private_training_class;
    }

    public Integer getServices_units() {
        return services_units;
    }

    public void setServices_units(Integer services_units) {
        this.services_units = services_units;
    }

    public Service_Codes getService_codes() {
        return service_codes;
    }

    public void setService_codes(Service_Codes service_codes) {
        this.service_codes = service_codes;
    }
}
