package com.tamdesk.domain.customer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tamdesk.domain.AbstractAuditingEntity;
import com.tamdesk.domain.notes.Note;
import com.tamdesk.domain.project.Project;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;

/**
 * Created by psanghavi on 9/27/14.
 */

@Entity
@Table(name = "tamdesk_customer")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Customer extends AbstractAuditingEntity implements Serializable {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat.forPattern("MM/dd/yyyy H:mm:ss z");


    /*Important Information  about the customer. These are customers for an account. for example, if company xyz buys the tamdesk service then all the
    customers for xyz are modelled with this class   */

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "customer_id")
    private Integer customer_id;

    @NotNull
    @Size(min = 0, max = 255)
    private String Name;

    @Size(min = 0, max = 50)
    private String salesForceId;       //salesforce id of the account in salesforce. This is used to map contacts

    @Size(min = 0, max = 50)
    private String zendeskId;

    @Size(min = 0, max = 255)
    private String Total_Account_Revenue;

    private Boolean isSaas;
    private Boolean isDeleted;


    @Enumerated(EnumType.STRING)
    private Customer_Region customer_region;

    @Size(min = 0, max = 2048)
    private String saasURL;

    private String customerPriority ;
    private String licenseType;

    //General
    private String Type;
    private String premiumLevel;
    private String AnnualRevenue;

    //Customer Information - Billing address

    private String BillingStreet;
    private String BillingCity ;
    private String BillingState;
    private String BillingPostalCode;
    private String BillingCountry ;
    private String BillingLatitude ;
    private String BillingLongitude;

    //Team Information
    private String salesAccountMgr;
    private String salesEngineer;
    private String Technical_Account_Manager;
    private String Secondary_Technical_Account_Manager;
    private String Customer_Relationship_manager;
    private String Technical_Architect;
    private String ExecutiveSponsor;
    private String EBChampion;
    private String TechChampion;


    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime Date_TAM_Assigned_to_Account;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime Date_Architect_was_assigned_to_Account;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime Date_Secondary_TAM_was_assigned_to_Account;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime Date_CRM_was_assigned_to_Account;
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime  Last_Customer_Outreach;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime  Last_Date_of_Customer_Response;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime  New_Customer_Acquisition_Date;

    //Customer Statistics
    private String sentiment;
    private Double adoptionScore;
    private Double riskScore;
    private Double Customer_Satisfaction_Score;
    private Double NPS_Score;
    private String customer_status;

    @OneToMany(cascade=CascadeType.ALL)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<Customer_Opportunity> customer_opportunities;

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<Customer_Contacts> customer_contacts;


    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="deployment_status_id")
    @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private DeploymentStatus deploymentStatus;


    //customer environment details

    @OneToMany(cascade=CascadeType.ALL)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<EnvironmentDetail> environments;


    @OneToMany (cascade=CascadeType.ALL)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<DeploymentDetail> deployment_details;

    @OneToMany(cascade=CascadeType.ALL)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<SaaSEnvironment> saaSEnvironment;


    //Customer Documents
    @Size(min = 0, max = 2048)
    private String GoogleDriveURL;

    @Size(min = 0, max = 2048)
    private String SalesForceURL;

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<Project> projects;

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<Note> notes;

    public Integer getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Integer customer_id) {
        this.customer_id = customer_id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSalesForceId() {
        return salesForceId;
    }

    public void setSalesForceId(String salesForceId) {
        this.salesForceId = salesForceId;
    }

    public String getZendeskId() {
        return zendeskId;
    }

    public void setZendeskId(String zendeskId) {
        this.zendeskId = zendeskId;
    }

    public String getTotal_Account_Revenue() {
        return Total_Account_Revenue;
    }

    public void setTotal_Account_Revenue(String total_Account_Revenue) {
        Total_Account_Revenue = total_Account_Revenue;
    }

    public Boolean getIsSaas() {
        return isSaas;
    }

    public void setIsSaas(Boolean isSaas) {
        this.isSaas = isSaas;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Customer_Region getCustomer_region() {
        return customer_region;
    }

    public void setCustomer_region(Customer_Region customer_region) {
        this.customer_region = customer_region;
    }

    public String getSaasURL() {
        return saasURL;
    }

    public void setSaasURL(String saasURL) {
        this.saasURL = saasURL;
    }

    public String getCustomerPriority() {
        return customerPriority;
    }

    public void setCustomerPriority(String customerPriority) {
        this.customerPriority = customerPriority;
    }

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getPremiumLevel() {
        return premiumLevel;
    }

    public void setPremiumLevel(String premiumLevel) {
        this.premiumLevel = premiumLevel;
    }

    public String getAnnualRevenue() {
        return AnnualRevenue;
    }

    public void setAnnualRevenue(String annualRevenue) {
        AnnualRevenue = annualRevenue;
    }

    public String getBillingStreet() {
        return BillingStreet;
    }

    public void setBillingStreet(String billingStreet) {
        BillingStreet = billingStreet;
    }

    public String getBillingCity() {
        return BillingCity;
    }

    public void setBillingCity(String billingCity) {
        BillingCity = billingCity;
    }

    public String getBillingState() {
        return BillingState;
    }

    public void setBillingState(String billingState) {
        BillingState = billingState;
    }

    public String getBillingPostalCode() {
        return BillingPostalCode;
    }

    public void setBillingPostalCode(String billingPostalCode) {
        BillingPostalCode = billingPostalCode;
    }

    public String getBillingCountry() {
        return BillingCountry;
    }

    public void setBillingCountry(String billingCountry) {
        BillingCountry = billingCountry;
    }

    public String getBillingLatitude() {
        return BillingLatitude;
    }

    public void setBillingLatitude(String billingLatitude) {
        BillingLatitude = billingLatitude;
    }

    public String getBillingLongitude() {
        return BillingLongitude;
    }

    public void setBillingLongitude(String billingLongitude) {
        BillingLongitude = billingLongitude;
    }

    public String getSalesAccountMgr() {
        return salesAccountMgr;
    }

    public void setSalesAccountMgr(String salesAccountMgr) {
        this.salesAccountMgr = salesAccountMgr;
    }

    public String getSalesEngineer() {
        return salesEngineer;
    }

    public void setSalesEngineer(String salesEngineer) {
        this.salesEngineer = salesEngineer;
    }

    public String getTechnical_Account_Manager() {
        return Technical_Account_Manager;
    }

    public void setTechnical_Account_Manager(String technical_Account_Manager) {
        Technical_Account_Manager = technical_Account_Manager;
    }

    public String getSecondary_Technical_Account_Manager() {
        return Secondary_Technical_Account_Manager;
    }

    public void setSecondary_Technical_Account_Manager(String secondary_Technical_Account_Manager) {
        Secondary_Technical_Account_Manager = secondary_Technical_Account_Manager;
    }

    public String getCustomer_Relationship_manager() {
        return Customer_Relationship_manager;
    }

    public void setCustomer_Relationship_manager(String customer_Relationship_manager) {
        Customer_Relationship_manager = customer_Relationship_manager;
    }

    public String getTechnical_Architect() {
        return Technical_Architect;
    }

    public void setTechnical_Architect(String technical_Architect) {
        Technical_Architect = technical_Architect;
    }

    public String getExecutiveSponsor() {
        return ExecutiveSponsor;
    }

    public void setExecutiveSponsor(String executiveSponsor) {
        ExecutiveSponsor = executiveSponsor;
    }

    public String getEBChampion() {
        return EBChampion;
    }

    public void setEBChampion(String EBChampion) {
        this.EBChampion = EBChampion;
    }

    public String getTechChampion() {
        return TechChampion;
    }

    public void setTechChampion(String techChampion) {
        TechChampion = techChampion;
    }

    public LocalDateTime getDate_TAM_Assigned_to_Account() {
        return Date_TAM_Assigned_to_Account;
    }

    public void setDate_TAM_Assigned_to_Account(LocalDateTime date_TAM_Assigned_to_Account) {
        Date_TAM_Assigned_to_Account = date_TAM_Assigned_to_Account;
    }

    public LocalDateTime getDate_Architect_was_assigned_to_Account() {
        return Date_Architect_was_assigned_to_Account;
    }

    public void setDate_Architect_was_assigned_to_Account(LocalDateTime date_Architect_was_assigned_to_Account) {
        Date_Architect_was_assigned_to_Account = date_Architect_was_assigned_to_Account;
    }

    public LocalDateTime getDate_Secondary_TAM_was_assigned_to_Account() {
        return Date_Secondary_TAM_was_assigned_to_Account;
    }

    public void setDate_Secondary_TAM_was_assigned_to_Account(LocalDateTime date_Secondary_TAM_was_assigned_to_Account) {
        Date_Secondary_TAM_was_assigned_to_Account = date_Secondary_TAM_was_assigned_to_Account;
    }

    public LocalDateTime getDate_CRM_was_assigned_to_Account() {
        return Date_CRM_was_assigned_to_Account;
    }

    public void setDate_CRM_was_assigned_to_Account(LocalDateTime date_CRM_was_assigned_to_Account) {
        Date_CRM_was_assigned_to_Account = date_CRM_was_assigned_to_Account;
    }

    public LocalDateTime getLast_Customer_Outreach() {
        return Last_Customer_Outreach;
    }

    public void setLast_Customer_Outreach(LocalDateTime last_Customer_Outreach) {
        Last_Customer_Outreach = last_Customer_Outreach;
    }

    public LocalDateTime getLast_Date_of_Customer_Response() {
        return Last_Date_of_Customer_Response;
    }

    public void setLast_Date_of_Customer_Response(LocalDateTime last_Date_of_Customer_Response) {
        Last_Date_of_Customer_Response = last_Date_of_Customer_Response;
    }

    public LocalDateTime getNew_Customer_Acquisition_Date() {
        return New_Customer_Acquisition_Date;
    }

    public void setNew_Customer_Acquisition_Date(LocalDateTime new_Customer_Acquisition_Date) {
        New_Customer_Acquisition_Date = new_Customer_Acquisition_Date;
    }

    public String getSentiment() {
        return sentiment;
    }

    public void setSentiment(String sentiment) {
        this.sentiment = sentiment;
    }

    public Double getAdoptionScore() {
        return adoptionScore;
    }

    public void setAdoptionScore(Double adoptionScore) {
        this.adoptionScore = adoptionScore;
    }

    public Double getRiskScore() {
        return riskScore;
    }

    public void setRiskScore(Double riskScore) {
        this.riskScore = riskScore;
    }

    public Double getCustomer_Satisfaction_Score() {
        return Customer_Satisfaction_Score;
    }

    public void setCustomer_Satisfaction_Score(Double customer_Satisfaction_Score) {
        Customer_Satisfaction_Score = customer_Satisfaction_Score;
    }

    public Double getNPS_Score() {
        return NPS_Score;
    }

    public void setNPS_Score(Double NPS_Score) {
        this.NPS_Score = NPS_Score;
    }

    public String getCustomer_status() {
        return customer_status;
    }

    public void setCustomer_status(String customer_status) {
        this.customer_status = customer_status;
    }

    public List<Customer_Contacts> getCustomer_contacts() {
        return customer_contacts;
    }

    public void setCustomer_contacts(List<Customer_Contacts> customer_contacts) {
        this.customer_contacts = customer_contacts;
    }

    public DeploymentStatus getDeploymentStatus() {
        return deploymentStatus;
    }

    public void setDeploymentStatus(DeploymentStatus deploymentStatus) {
        this.deploymentStatus = deploymentStatus;
    }

    public List<EnvironmentDetail> getEnvironments() {
        return environments;
    }

    public void setEnvironments(List<EnvironmentDetail> environments) {
        this.environments = environments;
    }

    public List<DeploymentDetail> getDeployment_details() {
        return deployment_details;
    }

    public void setDeployment_details(List<DeploymentDetail> deployment_details) {
        this.deployment_details = deployment_details;
    }

    public List<SaaSEnvironment> getSaaSEnvironment() {
        return saaSEnvironment;
    }

    public void setSaaSEnvironment(List<SaaSEnvironment> saaSEnvironment) {
        this.saaSEnvironment = saaSEnvironment;
    }

    public String getGoogleDriveURL() {
        return GoogleDriveURL;
    }

    public void setGoogleDriveURL(String googleDriveURL) {
        GoogleDriveURL = googleDriveURL;
    }

    public String getSalesForceURL() {
        return SalesForceURL;
    }

    public void setSalesForceURL(String salesForceURL) {
        SalesForceURL = salesForceURL;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<Customer_Opportunity> getCustomer_opportunities() {
        return customer_opportunities;
    }

    public void setCustomer_opportunities(List<Customer_Opportunity> customer_opportunities) {
        this.customer_opportunities = customer_opportunities;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }


}





