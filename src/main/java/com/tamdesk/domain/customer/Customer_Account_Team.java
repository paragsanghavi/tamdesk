package com.tamdesk.domain.customer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by psanghavi on 11/17/14.
 */


@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Customer_Account_Team {

    private Integer customer_id;
    private String customer_name;
    private String primary_tam;
    private String secondary_tam;
    private String sales_rep;
    private String sales_engineer;
    private String technical_architect;
    private String customer_relationship_manager;
    private String executive_sponsor;

    public Integer getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Integer customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getPrimary_tam() {
        return primary_tam;
    }

    public void setPrimary_tam(String primary_tam) {
        this.primary_tam = primary_tam;
    }

    public String getSecondary_tam() {
        return secondary_tam;
    }

    public void setSecondary_tam(String secondary_tam) {
        this.secondary_tam = secondary_tam;
    }

    public String getSales_rep() {
        return sales_rep;
    }

    public void setSales_rep(String sales_rep) {
        this.sales_rep = sales_rep;
    }

    public String getSales_engineer() {
        return sales_engineer;
    }

    public void setSales_engineer(String sales_engineer) {
        this.sales_engineer = sales_engineer;
    }

    public String getTechnical_architect() {
        return technical_architect;
    }

    public void setTechnical_architect(String technical_architect) {
        this.technical_architect = technical_architect;
    }

    public String getCustomer_relationship_manager() {
        return customer_relationship_manager;
    }

    public void setCustomer_relationship_manager(String customer_relationship_manager) {
        this.customer_relationship_manager = customer_relationship_manager;
    }

    public String getExecutive_sponsor() {
        return executive_sponsor;
    }

    public void setExecutive_sponsor(String executive_sponsor) {
        this.executive_sponsor = executive_sponsor;
    }
}
