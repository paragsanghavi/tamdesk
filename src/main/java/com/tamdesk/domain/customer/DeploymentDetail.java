package com.tamdesk.domain.customer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by psanghavi on 9/27/14.
 */

@Entity
@Table(name = "tamdesk_customer_deployment_details")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)

public class DeploymentDetail {

    @NotNull
    @Id
    @Column(name = "deployment_detail_id")
    private Integer deployment_detail_id;

    @ManyToOne
    @JoinColumn(name="customer_id")
    private Customer customer;

    private String envName;
    private boolean status;

    public Integer getDeployment_detail_id() {
        return deployment_detail_id;
    }

    public void setDeployment_detail_id(Integer deployment_detail_id) {
        this.deployment_detail_id = deployment_detail_id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getEnvName() {
        return envName;
    }

    public void setEnvName(String envName) {
        this.envName = envName;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}

