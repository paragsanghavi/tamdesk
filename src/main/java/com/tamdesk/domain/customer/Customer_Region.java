package com.tamdesk.domain.customer;

/**
 * Created by psanghavi on 10/5/14.
 */

public enum Customer_Region {
    WEST, EAST, CENTRAL_SOUTH,EMEA,ROW
}
