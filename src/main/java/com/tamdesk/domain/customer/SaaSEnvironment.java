package com.tamdesk.domain.customer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by psanghavi on 9/27/14.
 */

@Entity
@Table(name = "tamdesk_customer_saas_environment_detail")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)

public class SaaSEnvironment {


    @NotNull
    @Id
    @Column(name = "saas_environment_id")
    private Integer saas_environment_id;

    @ManyToOne
    @JoinColumn(name="customer_id")
    private Customer customer;

    private String url;
    private String user;
    private String password;

    public Integer getSaas_environment_id() {
        return saas_environment_id;
    }

    public void setSaas_environment_id(Integer saas_environment_id) {
        this.saas_environment_id = saas_environment_id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

