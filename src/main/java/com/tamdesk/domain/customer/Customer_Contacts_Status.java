package com.tamdesk.domain.customer;

/**
 * Created by psanghavi on 10/5/14.
 */
public enum Customer_Contacts_Status {

    ACTIVE, INACTIVE, LOCKED
}
