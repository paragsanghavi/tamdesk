package com.tamdesk.domain.customer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by psanghavi on 9/27/14.
 */

@Entity
@Table(name = "tamdesk_customer_opportunity")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)

public class Customer_Opportunity {
    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "opportunity_id")
    private Integer opportunity_id;

    private String Opportunity_name;
    private String account_name;

    @Size(min = 0, max = 50)
    private String salesforceAccountId;

    @Size(min = 0, max = 50)
    private String salesforceId;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="customer_id")
    private Customer customer;

    private String amount ;
    private String Closed_date;
    private String Opportunity_owner;
    private String Opportunity_stage;
    private Integer Project_score; //  this is to track how different business groups are doing
    private String region;
    private String Probability;
    private Boolean isDeleted;
    private Boolean isWon;
    private String product_code;


    @Size(min = 0, max = 1024)
    private String nextStep;

    @Size(min = 0, max = 5000)
    private String Description ;

    public Integer getOpportunity_id() {
        return opportunity_id;
    }

    public void setOpportunity_id(Integer opportunity_id) {
        this.opportunity_id = opportunity_id;
    }

    public String getOpportunity_name() {
        return Opportunity_name;
    }

    public void setOpportunity_name(String opportunity_name) {
        Opportunity_name = opportunity_name;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getSalesforceAccountId() {
        return salesforceAccountId;
    }

    public void setSalesforceAccountId(String salesforceAccountId) {
        this.salesforceAccountId = salesforceAccountId;
    }

    public String getSalesforceId() {
        return salesforceId;
    }

    public void setSalesforceId(String salesforceId) {
        this.salesforceId = salesforceId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getClosed_date() {
        return Closed_date;
    }

    public void setClosed_date(String closed_date) {
        Closed_date = closed_date;
    }

    public String getOpportunity_owner() {
        return Opportunity_owner;
    }

    public void setOpportunity_owner(String opportunity_owner) {
        Opportunity_owner = opportunity_owner;
    }

    public String getOpportunity_stage() {
        return Opportunity_stage;
    }

    public void setOpportunity_stage(String opportunity_stage) {
        Opportunity_stage = opportunity_stage;
    }

    public Integer getProject_score() {
        return Project_score;
    }

    public void setProject_score(Integer project_score) {
        Project_score = project_score;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getProbability() {
        return Probability;
    }

    public void setProbability(String probability) {
        Probability = probability;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Boolean getIsWon() {
        return isWon;
    }

    public void setIsWon(Boolean isWon) {
        this.isWon = isWon;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getNextStep() {
        return nextStep;
    }

    public void setNextStep(String nextStep) {
        this.nextStep = nextStep;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
}
