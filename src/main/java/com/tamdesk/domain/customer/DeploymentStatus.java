package com.tamdesk.domain.customer;

/**
 * Created by psanghavi on 9/27/14.
 */
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Table(name = "tamdesk_customer_deployment_status")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)

public class DeploymentStatus {

    @NotNull
    @Id
    @Column(name = "deployment_status_id")
    private Integer deployment_status_id;

    @OneToOne(mappedBy="deploymentStatus")
    private Customer customer;

    private Double percentage;

    public Integer getDeployment_status_id() {
        return deployment_status_id;
    }

    public void setDeployment_status_id(Integer deployment_status_id) {
        this.deployment_status_id = deployment_status_id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Double getPercentage() {
        return percentage;
    }

    public void setPercentage(Double percentage) {
        this.percentage = percentage;
    }
}