package com.tamdesk.domain.customer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;
/**
 * Created by psanghavi on 9/27/14.
 */

@Entity
@Table(name = "tamdesk_customer_environment_details")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)

public class EnvironmentDetail {

    @NotNull
    @Id
    @Column(name = "environment_detail_id")
    private Integer environment_detail_id;

    @ManyToOne
    @JoinColumn(name="customer_id")
    private Customer customer;

    private String name;

    @ElementCollection(targetClass=String.class)
    private List<String> details;

    public Integer getEnvironment_detail_id() {
        return environment_detail_id;
    }

    public void setEnvironment_detail_id(Integer environment_detail_id) {
        this.environment_detail_id = environment_detail_id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getDetails() {
        return details;
    }

    public void setDetails(List<String> details) {
        this.details = details;
    }
}
