package com.tamdesk.domain.productuser;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by psanghavi on 9/28/14.
 */

@Entity
@Table(name = "tamdesk_user")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)

public class productUser {


    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Size(min = 0, max = 50)
    private String salesforceAccountId;

    @Size(min = 0, max = 50)
    private String salesforceId;

    @Size(min = 0, max = 50)
    private String tamdeskAccountId;

    private String name;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String title;
    private String username;

    private String profileImageId;
    @Size(min = 0, max = 1024)
    private String photoUrl;
    private String phone;
    private String skypeId;
    private Boolean IsDeleted;
    private Boolean IsActive;

    @Enumerated(EnumType.STRING)
    productUserType type;

    @Enumerated(EnumType.STRING)
    productUserRole role;


    @Enumerated(EnumType.STRING)
    private productUserStatus Status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSalesforceAccountId() {
        return salesforceAccountId;
    }

    public void setSalesforceAccountId(String salesforceAccountId) {
        this.salesforceAccountId = salesforceAccountId;
    }

    public String getSalesforceId() {
        return salesforceId;
    }

    public void setSalesforceId(String salesforceId) {
        this.salesforceId = salesforceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public productUserStatus getStatus() {
        return Status;
    }

    public void setStatus(productUserStatus status) {
        Status = status;
    }

    public String getProfileImageId() {
        return profileImageId;
    }

    public void setProfileImageId(String profileImageId) {
        this.profileImageId = profileImageId;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSkypeId() {
        return skypeId;
    }

    public void setSkypeId(String skypeId) {
        this.skypeId = skypeId;
    }

    public productUserType getType() {
        return type;
    }

    public void setType(productUserType type) {
        this.type = type;
    }

    public productUserRole getRole() {
        return role;
    }

    public void setRole(productUserRole role) {
        this.role = role;
    }

    public String getTamdeskAccountId() {
        return tamdeskAccountId;
    }

    public void setTamdeskAccountId(String tamdeskAccountId) {
        this.tamdeskAccountId = tamdeskAccountId;
    }

    public Boolean getIsDeleted() {
        return IsDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        IsDeleted = isDeleted;
    }

    public Boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(Boolean isActive) {
        IsActive = isActive;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
