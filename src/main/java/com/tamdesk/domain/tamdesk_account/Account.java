package com.tamdesk.domain.tamdesk_account;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tamdesk.domain.AbstractAuditingEntity;
import com.tamdesk.domain.notes.Note;
import com.tamdesk.domain.project.Project;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;

/**
 * Created by psanghavi on 11/17/14.
 *
 * This is the main class that has info on the Tamdesk customer
 */
public class Account extends AbstractAuditingEntity implements Serializable {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "account_id")
    private Integer account_id;

    @NotNull
    @Size(min = 0, max = 255)
    private String sfUsername;


    @NotNull
    @Size(min = 0, max = 255)
    private String sfPassword;

    @NotNull
    @Size(min = 0, max = 255)
    private String helpdeskUsername;

    @NotNull
    @Size(min = 0, max = 255)
    private String helpdeskPassword;

    public Integer getAccount_id() {
        return account_id;
    }

    public void setAccount_id(Integer account_id) {
        this.account_id = account_id;
    }

    public String getSfUsername() {
        return sfUsername;
    }

    public void setSfUsername(String sfUsername) {
        this.sfUsername = sfUsername;
    }

    public String getSfPassword() {
        return sfPassword;
    }

    public void setSfPassword(String sfPassword) {
        this.sfPassword = sfPassword;
    }

    public String getHelpdeskUsername() {
        return helpdeskUsername;
    }

    public void setHelpdeskUsername(String helpdeskUsername) {
        this.helpdeskUsername = helpdeskUsername;
    }

    public String getHelpdeskPassword() {
        return helpdeskPassword;
    }

    public void setHelpdeskPassword(String helpdeskPassword) {
        this.helpdeskPassword = helpdeskPassword;
    }
}
