package com.tamdesk.service.customer;
import com.tamdesk.domain.customer.Customer;
import com.tamdesk.repository.customer.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by psanghavi on 9/23/14.
 */

@Service
@Transactional

public class CustomerService {

    private final Logger log = LoggerFactory.getLogger(CustomerService.class);


    @Inject
    private CustomerRepository customerRepository;

    public void create(Customer customer){

        customerRepository.save(customer);
        log.debug("Created customer :  {}", customer.getName());

    }

    public void updateCustomerInformation(Customer customer){

        //Customer existing_customer = customerRepository.findOne(customer.getId());

        Customer existing_customer = customerRepository.getCustomerByCustomerId(customer.getCustomer_id());

        existing_customer.setName(customer.getName());
        existing_customer.setAdoptionScore(customer.getAdoptionScore());
        existing_customer.setCustomer_Relationship_manager(customer.getCustomer_Relationship_manager());
        existing_customer.setTechnical_Account_Manager(customer.getTechnical_Account_Manager());
        existing_customer.setCustomer_Satisfaction_Score(customer.getCustomer_Satisfaction_Score());
        existing_customer.setCustomer_status(customer.getCustomer_status());
        existing_customer.setSalesAccountMgr(customer.getSalesAccountMgr());
        existing_customer.setCustomerPriority(customer.getCustomerPriority());

        existing_customer.setSalesEngineer(customer.getSalesEngineer());
        existing_customer.setSecondary_Technical_Account_Manager(customer.getSecondary_Technical_Account_Manager());
        existing_customer.setTechChampion(customer.getTechChampion());
        existing_customer.setGoogleDriveURL(customer.getGoogleDriveURL());
        existing_customer.setExecutiveSponsor(customer.getExecutiveSponsor());
        existing_customer.setCustomer_Satisfaction_Score(customer.getCustomer_Satisfaction_Score());
        existing_customer.setEBChampion(customer.getEBChampion());
        existing_customer.setTechnical_Architect(customer.getTechnical_Architect());
        existing_customer.setIsSaas(customer.getIsSaas());
        existing_customer.setNPS_Score(customer.getNPS_Score());
        existing_customer.setDeploymentStatus(customer.getDeploymentStatus());
        existing_customer.setLicenseType(customer.getLicenseType());
        existing_customer.setEnvironments(customer.getEnvironments());

        existing_customer.setNew_Customer_Acquisition_Date(customer.getNew_Customer_Acquisition_Date());
        existing_customer.setDate_TAM_Assigned_to_Account(customer.getDate_TAM_Assigned_to_Account());
        existing_customer.setDate_Secondary_TAM_was_assigned_to_Account(customer.getDate_Secondary_TAM_was_assigned_to_Account());
        existing_customer.setDate_CRM_was_assigned_to_Account(customer.getDate_CRM_was_assigned_to_Account());
        existing_customer.setDate_Architect_was_assigned_to_Account(customer.getDate_Architect_was_assigned_to_Account());

        existing_customer.setBillingStreet(customer.getBillingStreet());
        existing_customer.setBillingCity(customer.getBillingCity());
        existing_customer.setBillingCountry(customer.getBillingCountry());
        existing_customer.setBillingPostalCode(customer.getBillingPostalCode());
        existing_customer.setBillingState(customer.getBillingState());
        existing_customer.setCustomer_region(customer.getCustomer_region());
        customerRepository.save(existing_customer);
        log.debug("Changed Information for Customer: {}", existing_customer.getName());
    }
    public Customer getCustomerByCustomerId(Integer customerId){

        log.debug("Getting customer Information for Id: {}", customerId);

        return customerRepository.getCustomerByCustomerId(customerId)  ;
    }


    public Customer getCustomerBySalesForceId(String salesForceId){

        return customerRepository.getCustomerBySalesForceId(salesForceId)  ;

    }


    public List<Customer> top10CustomerByRevenue( ){
        return customerRepository.top10CustomerByRevenue();
    }


    public List<Customer> top10CustomerByAdoptionScore( ){
        return customerRepository.top10CustomerByAdoptionScore();
    }


    public List<Customer> top10CustomerByRiskScore( ){
        return customerRepository.top10CustomerByRiskScore()  ;
    }


    public long getCount( ) {
        return customerRepository.getCount();
    }


    public Page<Customer> getCustomers( Pageable pagebale){
        return customerRepository.getCustomers(pagebale);

    }

    public List<Customer> getallCustomers ( ){
        return customerRepository.getallCustomers();

    }

}
