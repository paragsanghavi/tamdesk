package com.tamdesk.service.customer;

import com.tamdesk.domain.customer.Customer_Opportunity;
import com.tamdesk.repository.customer.CustomerOpportunityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by psanghavi on 10/5/14.
 */

@Service
@Transactional
public class CustomerOpportunityService {

    private final Logger log = LoggerFactory.getLogger(CustomerOpportunityService.class);


    @Inject
    private CustomerOpportunityRepository customeropportunityRepository;

    public Customer_Opportunity getOpportunityById(String opportunityId){
        return customeropportunityRepository.getOpportunityById(opportunityId);

    }


    public Page<Customer_Opportunity> getOpportunityBySalesForceId(String salesforceAccountId, Pageable pageable){

        return customeropportunityRepository.getOpportunityBySalesForceId(salesforceAccountId,pageable);

    }


    public Page<Customer_Opportunity> getOpportunities(Integer tamdeskAccountId, Pageable pageable){
        return customeropportunityRepository.getOpportunities(tamdeskAccountId,pageable);
    }

    public List<Customer_Opportunity> getOpportunities(Integer tamdeskAccountId){
        return customeropportunityRepository.getOpportunities(tamdeskAccountId);
    }


}
