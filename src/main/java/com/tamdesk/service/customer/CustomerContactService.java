package com.tamdesk.service.customer;


import com.tamdesk.domain.customer.Customer_Contacts;
import com.tamdesk.repository.customer.CustomerContactsRepository;
import com.tamdesk.repository.customer.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by psanghavi on 10/5/14.
 */

@Service
@Transactional
public class CustomerContactService {

    private final Logger log = LoggerFactory.getLogger(CustomerContactService.class);


    @Inject
    private CustomerContactsRepository customercontactRepository;

    public Customer_Contacts getCustomerByCustomerId(String contactId){
        return customercontactRepository.getCustomerByCustomerId(contactId);
    }

   public Page<Customer_Contacts> getCustomerBySalesForceId(String salesforceAccountId,Pageable pageable){
       return customercontactRepository.getCustomerBySalesForceId(salesforceAccountId,pageable) ;
   }


    public Page<Customer_Contacts> getCustomerContacts(String tamdeskAccountId, Pageable pageable){
        return customercontactRepository.getCustomerContacts(tamdeskAccountId,pageable);
    }


    public List<Customer_Contacts> getCustomerContactsByCustomerId (Integer tamdeskAccountId ){
        return customercontactRepository.getCustomerContactsByCustomerId(tamdeskAccountId);

    }
}
