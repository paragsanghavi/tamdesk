package com.tamdesk.service.notes;
import com.tamdesk.domain.notes.Note;
import com.tamdesk.domain.notes.Notes;
import com.tamdesk.repository.notes.NoteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

/**
 * Created by psanghavi on 9/23/14.
 */

@Service
@Transactional
public class NoteService {


    private final Logger log = LoggerFactory.getLogger(NoteService.class);


    @Inject
    private NoteRepository noteRepository;


    public void create(Note note){

        noteRepository.save(note);
        log.debug("Created Note for Account Id : {}", note.getCustomer().getCustomer_id());

    }

    public void updateNoteInformation(Note note) {
        Note existing_note = noteRepository.getNoteByNoteId(note.getNote_id());
        existing_note.setContent(note.getContent());

        existing_note.setIsCustomerFacingNote(note.getIsCustomerFacingNote());
        existing_note.setNotetype(note.getNotetype());
        existing_note.setIsStickyNote(note.getIsStickyNote());
        existing_note.setLastmodifiedat(note.getLastmodifiedat());
        existing_note.setTitle(note.getTitle());
        existing_note.setTags(note.getTags());
        existing_note.setLastModifiedBy(note.getLastModifiedBy());

        noteRepository.save(existing_note);
        log.debug("Changed Information for Note: {}", existing_note);
    }


    public Note getNote(String id){
         return new Note();
    }

    public Notes getNotes(String account_id) {

        Notes notes = new Notes();
        notes.setNotes( noteRepository.findNotesByAccountId(account_id) );
        return notes;

    }


    public Long getNotesCount(String accountId){
            return noteRepository.getCount(accountId);
    }

    public void delete(String accountId,String id){
         noteRepository.delete(id);
        log.debug("Deleted note : {}", id, accountId);
    }
}
