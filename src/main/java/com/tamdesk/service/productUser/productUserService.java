package com.tamdesk.service.productUser;

import com.tamdesk.domain.productuser.productUser;
import com.tamdesk.repository.productUser.productUserRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by psanghavi on 10/5/14.
 */

@Service
@Transactional
public class productUserService {

    private final Logger log = LoggerFactory.getLogger(productUserService.class);


    @Inject
    private productUserRepository productuserRepository;

    public productUser getUserById(String userId){
        return productuserRepository.getUserById(userId);
    }

    public Page<productUser> getproductUsersBySalesForceAccountId(String salesforceAccountId,Pageable pageable){
        return productuserRepository.getproductUsersBySalesForceAccountId(salesforceAccountId,pageable) ;
    }


    public Page<productUser> getAllProductUsersByTamDeskID(String tamdeskAccountId, Pageable pageable){
        return productuserRepository.getAllProductUsersByTamDeskID(tamdeskAccountId,pageable);
    }

    public List<productUser> getAllProductUsers(){
        return productuserRepository.getAllProductUsers();
    }
}
