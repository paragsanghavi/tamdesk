package com.tamdesk.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
/**
 * Created by psanghavi on 10/4/14.
 */


@Configuration
@EnableSpringDataWebSupport
@ComponentScan(basePackages = {"com.tamdesk.web"})

public class WebConfig {

}