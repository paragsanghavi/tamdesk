'use strict';

mytamdesk.factory('getProjectActivityType', function ($resource) {
                   return $resource('/tamdesk/rest/accounts/getProjectActivityType', {}, {
                  'get': { method: 'GET', params: {}, isArray: true}
                  });
});
mytamdesk.factory('getProjectActivityLocation', function ($resource) {
                   return $resource('/tamdesk/rest/accounts/getProjectActivityLocation', {}, {
                  'get': { method: 'GET', params: {}, isArray: true}
                  });
});
mytamdesk.factory('getProjectActivityStatus', function ($resource) {
                   return $resource('/tamdesk/rest/accounts/getProjectActivityStatus', {}, {
                  'get': { method: 'GET', params: {}, isArray: true}
                  });
});

mytamdesk.factory('getproductUsers', function ($resource) {
                   return $resource('/tamdesk/rest/productUsers', {}, {
                  'get': { method: 'GET', params: {}, isArray: true}
                  });
});

mytamdesk.factory('createProjectActivity', function ($resource) {
                  return $resource('/tamdesk/rest/project/createactivity/:projectId/create', {}, {
                  'createActivity': { method: 'POST',  isArray: false, headers: {'Content-Type': 'application/json'},params: {}}
                  });
              });

angular.module('mytamdesk').controller('ProjectActivityCtrl',function($scope,$filter,$location,$routeParams,projectService,getProjectActivityType,getProjectActivityLocation,getProjectActivityStatus,getproductUsers,createProjectActivity){

    $scope.project=projectService.project();
     //get all activity type
    $scope.allProjectActivityType = getProjectActivityType.get();
     //console.log($scope.allProjectActivityType) ;

    //get all activity status
    $scope.allProjectActivityStatus = getProjectActivityStatus.get();

     //get all activity Location
    $scope.allProjectActivityLocation = getProjectActivityLocation.get();

    //get productusers
    $scope.usersObjects = [];
    $scope.users = [];
    $scope.getAllproductUsers = getproductUsers.get(function(response) {

           angular.forEach(response, function (item) {
               if (item) {
                   //console.log(item.name)
                   $scope.usersObjects.push(item);
                   $scope.users.push(item.name);
               }
           });
    });
    $scope.findUserId=function (username) {
                 angular.forEach($scope.usersObjects, function (user) {
                     if (user.name === username){
                        $scope.username_id=user.id;
                     }
                 });
             }

    $scope.saveactivity=  function(){
        $scope.projectActivity={}
        console.log($scope.activityType) ;
        console.log($scope.activityType.name) ;
        console.log($scope.tamUser) ;
        console.log($scope.description) ;
        console.log($scope.startdate);
        console.log($scope.enddate);
        console.log($scope.location) ;
        console.log($scope.status);
        $scope.findUserId($scope.tamUser);
        console.log($scope.username_id) ;

        $scope.projectActivity.project_activity_type=$scope.activityType.name;
        $scope.projectActivity.username=$scope.tamUser;
        $scope.projectActivity.username_id= $scope.username_id;
        $scope.projectActivity.activity_description=$scope.description
        $scope.projectActivity.project_id=$scope.project.id

        var _startdate = $filter('date')($scope.startdate, 'yyyy-MM-dd HH:mm:ss');
        var _enddate = $filter('date')($scope.enddate, 'yyyy-MM-dd HH:mm:ss');

        $scope.projectActivity.activity_start_date =_startdate;
        $scope.projectActivity.activity_end_date =  _enddate;
        $scope.projectActivity.project_activity_location= $scope.location.name;
        $scope.projectActivity.project_activity_status=$scope.status.name

        console.log($scope.projectActivity) ;

        createProjectActivity.createActivity({ projectId:$scope.project.id },$scope.projectActivity, function() {
            //data saved. do something here.
            //console.log("saved  activity")
           });
    }

    $scope.cancel=  function(){

    }

    $scope.onchange=function(item){
       $scope.activitytype=item;
       //console.log($scope.activitytype) ;
    }
/***************************************************************************************************************/
   //Date picker functionality
    $scope.today = function() {
        $scope.startdate = new Date();
        $scope.enddate = new Date();
    };
   $scope.today();

   $scope.clear = function () {
     $scope.startdate = null;
     $scope.enddate=null;
   };

   // Disable weekend selection
   $scope.disabled = function(date, mode) {
     return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
   };

   $scope.toggleMin = function() {
     $scope.minDate = $scope.minDate ? null : new Date();
   };
   $scope.toggleMin();

   $scope.open = function($event) {
     $event.preventDefault();
     $event.stopPropagation();

     $scope.opened = true;
   };

   $scope.openstartdate = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened1 = true;
      };

   $scope.dateOptions = {
     formatYear: 'yy',
     startingDay: 1
   };

   $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
   $scope.format = $scope.formats[0];



});