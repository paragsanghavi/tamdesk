 'use strict';

mytamdesk.factory('updateCustomerProject', function ($resource) {
           return $resource('/tamdesk/rest/accounts/:customerId/project/update/:projectId', {}, {
           'updateProject': { method: 'PUT',  isArray: false, headers: {'Content-Type': 'application/json'},params: {}}
           });
});

mytamdesk.factory('getallProjectActivites', function ($resource) {
                   return $resource('/tamdesk/rest/project/:projectId/project_activity/all', {}, {
                  'get': { method: 'GET', params: {}, isArray: true}
                  });
});
angular.module('mytamdesk').controller('ProjectDetailsCtrl',function($scope,$rootScope,$routeParams,projectService,getallProjectActivites,updateCustomerProject){
    console.log('project inside ProjectDetailsCtrl')
    console.log($scope.project)

    $scope.project=projectService.project();
    console.log('project after service call ProjectDetailsCtrl')
    console.log($scope.project)

   //getProjectActivity
   $scope.getProjectActivity = function() {
       $scope.projectactivities = [];
       $scope.rowCollection = [];
       $scope.itemsByPage=8;
       $scope.getallprojectactivites = getallProjectActivites.get({projectId:$scope.project.id },function(response) {
               angular.forEach(response, function (item) {
                       if (item) {
                           $scope.projectactivities.push(item);
                           $scope.rowCollection.push(item);
                       }
               });
               console.log($scope.projectactivities)
       });
   }
     //get project activites
     $scope.getProjectActivity();

     $scope.projectstatus = [
         {value: 1, text: 'ACTIVE'},
         {value: 2, text: 'CLOSED'},
         {value: 3, text: 'PENDING CLOSURE'}
       ];
     $scope.EBMeetingScheduled = [
               {value: 1, text: 'NO'},
               {value: 2, text: 'YES'}
       ];
    $scope.projectadoptionstatus = [
              {value: 1, text: 'Normal'},
              {value: 2, text: 'Warning'},
              {value: 3, text: 'Critical'},
              {value: 4, text: 'Former Customer'}
       ];

    $scope.saveProject = function() {
        $scope.project.status= $scope.tempprojectstatus;
        $scope.project.project_adoption_status = $scope.tempprojectadoptionstatus ;
        //$scope.project.isEBMeetingScheduled =$scope.tempisEBMeetingScheduled;
        $scope.project.start_date = $filter('date')($scope.project.start_date, 'yyyy-MM-dd HH:mm:ss');
        $scope.project.end_date = $filter('date')($scope.project.end_date, 'yyyy-MM-dd HH:mm:ss');
        $scope.project.project_close_date = $filter('date')($scope.project.project_close_date, 'yyyy-MM-dd HH:mm:ss');
        $scope.project.project_extension_date = $filter('date')($scope.project.project_extension_date, 'yyyy-MM-dd HH:mm:ss');
        $scope.project.kickoff_date= $filter('date')($scope.project.kickoff_date, 'yyyy-MM-dd HH:mm:ss');
        $scope.project.transition_meeting_date = $filter('date')($scope.project.transition_meeting_date, 'yyyy-MM-dd HH:mm:ss');
        $scope.project.sixty_day_EBMeeting_date = $filter('date')($scope.project.sixty_day_EBMeeting_date, 'yyyy-MM-dd HH:mm:ss');
        $scope.project.one_twenty_day_EBMeeting_date = $filter('date')($scope.project.one_twenty_day_EBMeeting_date, 'yyyy-MM-dd HH:mm:ss');
        $scope.project.one_eighty_day_EBMeeting_date =$filter('date')($scope.project.one_eighty_day_EBMeeting_date, 'yyyy-MM-dd HH:mm:ss');
        //console.log($scope.project) ;
        updateCustomerProject.updateProject({ customerId:$scope.customerId,projectId:$scope.project.id },$scope.project, function() {
            //console.log("updated  project: " + $scope.project.id)
        });
    }
    $scope.createProjectActivity=function(){
        $scope.switchTabTo('projectactivity') ;
        //$scope.activeTab = 'projectactivity';
    }
    $scope.checkStatus = function(data) {
        console.log(data.text);
        $scope.tempprojectstatus=data.text;
    }
    $scope.checkProjectAdoptionStatus= function(data) {
         console.log(data.text);
         $scope.tempprojectadoptionstatus=data.text;

    }
    $scope.checkEBMeeting   = function(data) {
       console.log(data.text);
       $scope.tempisEBMeetingScheduled=data.text;

    }


});