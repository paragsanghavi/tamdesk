mytamdesk.factory('getNewProject', function ($resource) {
            return $resource('/tamdesk/rest/accounts/:customerId/project/new', {}, {
            'get': { method: 'GET', params: {}, isArray: false}
            });
        });

mytamdesk.factory('getallServiceProducts', function ($resource) {
                       return $resource('/tamdesk/rest/projects/getallServiceProducts', {}, {
                       'get': { method: 'GET', params: {}, isArray: true}
                       });
                   });

mytamdesk.factory('createServicesProject', function ($resource) {
                  return $resource('/tamdesk/rest/accounts/:customerId/project/create', {}, {
                  'saveProject': { method: 'POST',  isArray: false, headers: {'Content-Type': 'application/json'},params: {}}
                  });
              });





angular.module('mytamdesk').controller('ProjectCtrl',function($scope,$filter,$location,$routeParams,getNewProject,getallServiceProducts,createServicesProject,deleteCustomerProject){

       console.log ("Project control") ;
       console.log ("Account ID " + $scope.customerId) ;
       $scope.customerId     = $routeParams.id ;
       $scope.accountId     = $routeParams.id   ;


       //get new project info
       $scope.newproject = getNewProject.get({customerId:$scope.customerId});
       console.log("Getting a new project Object")
       console.log($scope.newproject) ;

       //get all service products
       $scope.allserviceproducts = getallServiceProducts.get();
       //console.log("getting all products\n ");
       //console.log( $scope.allserviceproducts);

       $scope.quantity=1;   //minimum quantity has to be one
       $scope.product=null;  //on selection change

       $scope.products=[] ;


        $scope.onchange=function(item){
           $scope.product=item;
           console.log($scope.product) ;
        }

        $scope.onquantitychange=function(quant){
         $scope.quantity = quant;
        }
        $scope.addproduct = function() {

           console.log("quantity purchased :" + $scope.quantity);
           var productObj={};  //to add product to project
           productObj.package_id=$scope.product.package_id;
           productObj.package_name=$scope.product.package_name;
           productObj.sku=$scope.product.sku;
           productObj.quantity_purchased= $scope.quantity;       //note this here
           productObj.service_days=$scope.product.service_days;
           productObj.architect_days=$scope.product.architect_days;
           productObj.service_warranty_days=$scope.product.service_warranty_days;
           productObj.product_warranty_days=$scope.product.product_warranty_days;
           productObj.sales_giveaway_days=$scope.product.sales_giveaway_days;
           productObj.public_training_class_seats=$scope.product.public_training_class_seats;
           productObj.private_training_class=$scope.product.private_training_class;
           productObj.services_units=$scope.product.services_units;

            $scope.products.push(productObj);
            console.log("adding product")
            console.log($scope.products);
            $scope.product=null;

        }
        $scope.deleteproduct  = function(index){

            if (index > -1) {
                $scope.products.splice(index, 1);
            }

        }


       $scope.activeProjectTab = 'step1';
       $scope.tabProjectTemplates = {
           step1:'partial/project/create_project_step1.html',
           step2:'partial/project/create_project_step2.html',
           step3:'partial/project/create_project_step3.html',
           step4:'partial/project/create_project_step4.html'

       };

       $scope.switchProjectTabTo = function (tabId) {
           $scope.activeProjectTab = tabId;

       };



         //Date picker
         $scope.today = function() {
             $scope.dt = new Date();
             $scope.newproject.start_date  = new Date();
           };
         $scope.today();

         $scope.clear = function () {
             $scope.dt = null;
             $scope.newproject.start_date=null;
           };

           // Disable weekend selection
           $scope.disabled = function(date, mode) {
             return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
           };

           $scope.toggleMin = function() {
             $scope.minDate = $scope.minDate ? null : new Date();
           };
           $scope.toggleMin();

           $scope.open = function($event) {
             $event.preventDefault();
             $event.stopPropagation();

             $scope.opened = true;
           };

           $scope.dateOptions = {
             formatYear: 'yy',
             startingDay: 1
           };

           $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
           $scope.format = $scope.formats[0];


         $scope.createProject =function(){

               $scope.newproject.salesforceAccountId =  $scope.opportunity.salesforceAccountId ;
               $scope.newproject.salesforce_opportunity_id =  $scope.opportunity.salesforceId;
               $scope.newproject.tamdesk_opportunity_id =  $scope.opportunity.opportunity_id  ;
              console.log("start date:" + $scope.newproject.start_date)
              //console.log($scope.newproject.start_date);
              var _startdate = $filter('date')($scope.newproject.start_date, 'yyyy-MM-dd HH:mm:ss');
              var _enddate = $filter('date')($scope.newproject.end_date, 'yyyy-MM-dd HH:mm:ss');

               console.log("_startdate" + _startdate);
               console.log("_end date" + _enddate);
               $scope.newproject.start_date =_startdate;
               $scope.newproject.end_date =  _enddate;

               $scope.newproject.services= $scope.products;

                console.log($scope.newproject) ;
                console.log($scope.opportunity) ;
                console.log("calling to create project")  ;



               createServicesProject.saveProject({ customerId:$scope.customerId },$scope.newproject, function() {
                //data saved. do something here.
                console.log("saved  project")
               });

               console.log($scope);
               $scope.activeTab = 'projects';
               var path =  '/tam/account/'+ $scope.accountId ;
               console.log(path);
               //$location.path(path);

         }

            //code to handle wizard steps

                $scope.steps = ['step1', 'step2', 'step3'];
                $scope.step = 0;


                $scope.isFirstStep = function() {
                    return $scope.step === 0;
                };

                $scope.isLastStep = function() {
                    return $scope.step === ($scope.steps.length - 1);
                };

                $scope.isCurrentStep = function(step) {
                    return $scope.step === step;
                };

                $scope.setCurrentStep = function(step) {
                    $scope.step = step;
                };

                $scope.getCurrentStep = function() {
                    return $scope.steps[$scope.step];
                };

                $scope.getNextLabel = function() {
                    return ($scope.isLastStep()) ? 'Submit' : 'Next';
                };

                $scope.handlePrevious = function() {
                    $scope.step -= ($scope.isFirstStep()) ? 0 : 1;
                    $scope.activeProjectTab = $scope.getCurrentStep();
                };

                $scope.handleNext = function() {
                    if($scope.isLastStep()) {
                        //alert($scope);
                        $scope.createProject();
                    } else {
                        $scope.step += 1;
                        $scope.activeProjectTab = $scope.getCurrentStep();
                    }
                };
});