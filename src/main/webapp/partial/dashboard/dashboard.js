'use strict';

//Factory methods for MainDashboardCtrl
mytamdesk.factory('GetCustomerCount', function ($resource) {
        return $resource('/tamdesk/rest/accounts/count', {}, {
        'get': { method: 'GET', params: {}, isArray: false}
        });
    });

mytamdesk.factory('top10Revenue', function ($resource) {
        return $resource('/tamdesk/rest/accounts/top10Revenue', {}, {
        'get': { method: 'GET', params: {}, isArray: true}
        });
    });

mytamdesk.factory('top10atRiskCustomer', function ($resource) {
            return $resource('/tamdesk/rest/accounts/top10atRiskCustomer', {}, {
            'get': { method: 'GET', params: {}, isArray: true}
            });
        });

mytamdesk.factory('top10CustomerByAdoptionScore', function ($resource) {
            return $resource('/tamdesk/rest/accounts/top10AdoptionScore', {}, {
            'get': { method: 'GET', params: {}, isArray: true}
            });
        });


 //Factory methods for CustomerDashboardCtrl
  mytamdesk.factory('allCustomers', function ($resource) {
              return $resource('/tamdesk/rest/getallCustomers', {}, {
              'get': { method: 'GET', params: {}, isArray: true}
              });
          });


//**********************Controllers************************************
//  MainDashboardCtrl
angular.module('mytamdesk').controller('MainDashboardCtrl',function($scope,GetCustomerCount,top10Revenue,top10atRiskCustomer,top10CustomerByAdoptionScore){
      $scope.customerCount = GetCustomerCount.get();
      //console.log($scope.customerCount);

      $scope.accounts = top10Revenue.get();
      //console.log($scope.accounts)

      $scope.riskaccounts = top10atRiskCustomer.get();
      //console.log($scope.riskaccounts)

      $scope.adoptionscoreaccounts = top10CustomerByAdoptionScore.get();
            //console.log($scope.adoptionscoreaccounts)

});

// CustomerDashboardCtrl
angular.module('mytamdesk').controller('CustomerDashboardCtrl1',function($scope,allCustomers){
       $scope.customers = allCustomers.get();
       //console.log($scope.customers);
       $scope.itemsByPage=15;

});

angular.module('mytamdesk').controller('CustomerDashboardCtrl', ['$scope', '$filter','allCustomers', function ($scope, $filter,allCustomers) {

    $scope.customers = [];
    $scope.rowCollection = [];
    $scope.itemsByPage=10;
    $scope.getallcustomers = allCustomers.get(function(response) {

         //console.log($scope.getallcustomers);
                     angular.forEach(response, function (item) {
                             if (item) {
                                 $scope.customers.push(item);
                                 $scope.rowCollection.push(item);
                             }
                     });
                     //console.log("Number of customers")
                     //console.log($scope.customers.length);
         });
}]);

