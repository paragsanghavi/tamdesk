'use strict';

mytamdesk.factory('getCustomerContacts', function ($resource) {
           return $resource('/tamdesk/rest/accounts/ContactsByCustomerId/:tamdeskAccountId', {}, {
           'get': { method: 'GET', params: {}, isArray: true}
           });
});

angular.module('mytamdesk').controller('AccountContactsCtrl',function($scope,$routeParams,getCustomerContacts){
    //getcontacts
    //console.log ($scope.customerId);

  $scope.customercontacts = [];
  $scope.rowCollection = [];
  $scope.itemsByPage=10;
  $scope.getallcustomercontacts = getCustomerContacts.get({tamdeskAccountId:$scope.customerId},function(response) {

       angular.forEach(response, function (item) {
           if (item) {
               $scope.customercontacts.push(item);
               $scope.rowCollection.push(item);
           }
       });
  });
});