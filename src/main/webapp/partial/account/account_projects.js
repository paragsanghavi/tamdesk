'use strict';

mytamdesk.factory('getOpportunities', function ($resource) {
            return $resource('/tamdesk/rest/accounts/opportunities/:tamdeskAccountId', {}, {
            'get': { method: 'GET', params: {}, isArray: true}
            });
        });


mytamdesk.factory('getProjects', function ($resource) {
            return $resource('/tamdesk/rest/accounts/:tamdeskAccountId/projects', {}, {
            'get': { method: 'GET', params: {}, isArray: true}
            });
});

mytamdesk.factory('deleteCustomerProject', function ($resource) {
              return $resource('/tamdesk/rest/projects/delete/:projectId', {}, {
              'deleteProject': { method: 'DELETE',  isArray: false, headers: {'Content-Type': 'application/json'},params: {}}
              });
 });

angular.module('mytamdesk').service('projectService', function () {
     var data = {};
     return {
         project:function () {
             return data;
         },
         addProject:function (project) {

             data=project;
         },
         deleteProject:function (id) {
             var oldProjects = data;
             data = [];
             angular.forEach(oldProjects, function (project) {
                 if (project.id !== id) data.push(project);
             });
         }
     };
});

angular.module('mytamdesk').controller('AccountProjectsCtrl',function($scope,$rootScope,$routeParams,projectService,getProjects,getOpportunities,deleteCustomerProject){

    //customer projects
    $scope.customerprojects = getProjects.get({tamdeskAccountId:$scope.customerId});

    $scope.opportunity = null;
    $scope.saveopp   = function(opp){
      $scope.opportunity=opp
    }

    $scope.deleteproject = function (_projectId) {
          alert('are you sure : ' + _projectId);
          deleteCustomerProject.deleteProject({projectId:_projectId });
    }

    $scope.projectdetails = function (_projectId,project) {
        console.log("In project details" + _projectId)
        projectService.addProject(project);
        $scope.project=project;
        //console.log(project)
        $scope.switchTabTo('projectdetails') ;
    }

    $scope.refreshprojects  = function(){
        $scope.customerprojects = getProjects.get({tamdeskAccountId:$scope.customerId});
    }

   //get customer opportunities
   $scope.customeropps = [];
   $scope.rowCollection = [];
   $scope.itemsByPage=15;
   $scope.getallcustomersopps = getOpportunities.get({tamdeskAccountId:$scope.customerId},function(response) {
        angular.forEach(response, function (item) {
                if (item) {
                    $scope.customeropps.push(item);
                    $scope.rowCollection.push(item);
                }
        });
        //console.log($scope.customeropps.length);
   });




});
