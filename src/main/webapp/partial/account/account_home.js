'use strict';
angular.module('mytamdesk').controller('HomeCtrl',function($scope,$routeParams){


        var gaugesPalette = ['#8dc63f', '#40bbea', '#ffba00', '#cc3f44'];

        // Data Sources for all charts
        var reqs_per_second_data = [
            { time: new Date("October 02, 2014 01:00:00"), reqs: 89 },
            { time: new Date("October 02, 2014 02:00:00"), reqs: 79 },
            { time: new Date("October 02, 2014 03:00:00"), reqs: 115 },
            { time: new Date("October 02, 2014 04:00:00"), reqs: 186 },
            { time: new Date("October 02, 2014 05:00:00"), reqs: 110 },
            { time: new Date("October 02, 2014 06:00:00"), reqs: 196 },
            { time: new Date("October 02, 2014 07:00:00"), reqs: 128 },
            { time: new Date("October 02, 2014 08:00:00"), reqs: 51 },
            { time: new Date("October 02, 2014 09:00:00"), reqs: 70 },
            { time: new Date("October 02, 2014 10:00:00"), reqs: 54 },
            { time: new Date("October 02, 2014 11:00:00"), reqs: 2 },
            { time: new Date("October 02, 2014 12:00:00"), reqs: 184 },
            { time: new Date("October 02, 2014 13:00:00"), reqs: 48 },
            { time: new Date("October 02, 2014 14:00:00"), reqs: 75 },
            { time: new Date("October 02, 2014 15:00:00"), reqs: 200 },
            { time: new Date("October 02, 2014 16:00:00"), reqs: 9 },
            { time: new Date("October 02, 2014 17:00:00"), reqs: 37 },
            { time: new Date("October 02, 2014 18:00:00"), reqs: 152 },
            { time: new Date("October 02, 2014 19:00:00"), reqs: 78 },
            { time: new Date("October 02, 2014 20:00:00"), reqs: 154 },
            { time: new Date("October 02, 2014 21:00:00"), reqs: 184 },
            { time: new Date("October 02, 2014 22:00:00"), reqs: 167 },
            { time: new Date("October 02, 2014 23:00:00"), reqs: 12 },
            { time: new Date("October 03, 2014 00:00:00"), reqs: 88 },
        ];
        // Requests per second gauge
                            $('#reqs-per-second').dxCircularGauge({
                                scale: {
                                    startValue: 0,
                                    endValue: 10,
                                    majorTick: {
                                        tickInterval: 1
                                    }
                                },
                                rangeContainer: {
                                    palette: 'pastel',
                                    width: 3,
                                    ranges: [
                                        {
                                            startValue: 0,
                                            endValue: 5,
                                            color: gaugesPalette[3]
                                        }, {
                                            startValue: 5,
                                            endValue: 8,
                                            color: gaugesPalette[1]
                                        }, {
                                            startValue: 8,
                                            endValue: 10,
                                            color: gaugesPalette[0]
                                        }
                                    ],
                                },
                                value: 4,
                                valueIndicator: {
                                    offset: 1,
                                    color: '#2c2e2f',
                                    spindleSize: 12
                                }
                            });

        // Requests per second chart
        $("#reqs-per-second-chart").dxChart({
            dataSource: reqs_per_second_data,
            commonPaneSettings: {
                border: {
                    visible: true,
                    color: '#f5f5f5'
                }
            },
            commonSeriesSettings: {
                type: "area",
                argumentField: "time",
                border: {
                    color: '#68b828',
                    width: 1,
                    visible: true
                }
            },
            series: [
                { valueField: "reqs", name: "Reqs per Second", color: '#68b828', opacity: .5 },
            ],
            commonAxisSettings: {
                label: {
                    visible: true
                },
                grid: {
                    visible: true,
                    color: '#f5f5f5'
                }
            },
            argumentAxis: {
                valueMarginsEnabled: false,
                label: {
                    customizeText: function (arg) {
                        return date('h:i A', arg.value);
                    }
                },
            },
            legend: {
                visible: false
            }
        });

 // Requests per second gauge
         $('#reqs-per-second1').dxCircularGauge({
             scale: {
                 startValue: 0,
                 endValue: 10,
                 majorTick: {
                     tickInterval: 1
                 }
             },
             rangeContainer: {
                 palette: 'pastel',
                 width: 3,
                 ranges: [
                     {
                         startValue: 0,
                         endValue: 5,
                         color: gaugesPalette[3]
                     }, {
                         startValue: 5,
                         endValue: 8,
                         color: gaugesPalette[1]
                     }, {
                         startValue: 8,
                         endValue: 10,
                         color: gaugesPalette[0]
                     }
                 ],
             },
             value: 4,
             valueIndicator: {
                 offset: 1,
                 color: '#2c2e2f',
                 spindleSize: 12
             }
         });

         // Requests per second chart
         $("#reqs-per-second-chart1").dxChart({
             dataSource: reqs_per_second_data,
             commonPaneSettings: {
                 border: {
                     visible: true,
                     color: '#f5f5f5'
                 }
             },
             commonSeriesSettings: {
                 type: "area",
                 argumentField: "time",
                 border: {
                     color: '#7c38bc',
                     width: 1,
                     visible: true
                 }
             },
             series: [
                 { valueField: "reqs", name: "Reqs per Second", color: '#7c38bc', opacity: .5 },
             ],
             commonAxisSettings: {
                 label: {
                     visible: true
                 },
                 grid: {
                     visible: true,
                     color: '#f5f5f5'
                 }
             },
             argumentAxis: {
                 valueMarginsEnabled: false,
                 label: {
                     customizeText: function (arg) {
                         return date('h:i A', arg.value);
                     }
                 },
             },
             legend: {
                 visible: false
             }
         });

});