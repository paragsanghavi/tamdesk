 'use strict';



mytamdesk.factory('getCustomerInfo', function ($resource) {
            return $resource('/tamdesk/rest/accountsbycustid/:customerId', {}, {
            'get': { method: 'GET', params: {}, isArray: false}
            });
        });

angular.module('mytamdesk').controller('AccountCtrl',function($scope,$filter,$location,$routeParams,getCustomerInfo){

       $scope.customerId     = $routeParams.id ;
       $scope.accountId     = $routeParams.id;

       //get customer info
       $scope.customerinfo = getCustomerInfo.get({customerId:$scope.customerId});

        $scope.activeTab = 'home';

        $scope.tabTemplates = {
            home: 'partial/account/account_home.html',
            projects: 'partial/account/account_projects.html',
            notes:'partial/account/account_notes.html',
            documents:'partial/account/account_documents.html',
            tasks:'partial/account/account_tasks.html' ,
            createproject:'partial/project/create_project.html',
            projectdetails:'partial/project/project_details.html',
            contacts:'partial/account/account_contacts.html',
            projectactivity:'partial/project/create_project_activity.html'
        };

        $scope.switchTabTo = function (tabId) {
            $scope.activeTab = tabId;
            /* other stuff to do */
        };
        /*$scope.$watch('activeTab', function() {
             alert('hey, activeTab has changed!');
         }); */

});

