angular.module('mytamdesk', ['ui.bootstrap','ui.utils','ngRoute','ngAnimate']);

angular.module('mytamdesk').config(function($routeProvider) {

    $routeProvider.when('/tam/accounts',{templateUrl: 'yes/accounts.html'});
    $routeProvider.when('/tam/account/:id',{templateUrl: 'partial/account/account.html'});
    $routeProvider.when('/tam/accounts',{templateUrl: 'partial/accounts/accounts.html'});
    $routeProvider.when('/tam/accounts/notes/:id',{templateUrl: 'partial/notes/notes.html'});
    $routeProvider.when('/tam/accounts/:acctid/notes/note/:noteid',{templateUrl: 'partial/note/note.html'});
    $routeProvider.when('/tam/account/:accountId/contacts',{templateUrl: 'partial/contacts/contacts.html'});
    $routeProvider.when('/tam/account/:accountId/tickets',{templateUrl: 'partial/tickets/tickets.html'});
    $routeProvider.when('/tam/account/:accountId/meetings',{templateUrl: 'partial/meetings/meetings.html'});
    $routeProvider.when('/tam/account/:accountId/momAdd',{templateUrl: 'partial/mom/mom.html'});
    $routeProvider.when('/tam/account/:accountId/meetingAdd',{templateUrl: 'partial/meetingadd/meetingadd.html'});
    $routeProvider.when('/tam/account/:accountId/myPlaybook',{templateUrl: 'partial/myplaybook/myplaybook.html'});
    $routeProvider.when('/tam/activitystream',{templateUrl: 'partial/activitystream/activitystream.html'});
    $routeProvider.when('/tam/adoptionreport',{templateUrl: 'partial/adoptionreport/adoptionreport.html'});
    $routeProvider.when('/tam/churnreport',{templateUrl: 'partial/churnreport/churnreport.html'});
    /* Add New Routes Above */
    $routeProvider.otherwise({redirectTo:'/home'});

});

angular.module('mytamdesk').run(function($rootScope) {

    $rootScope.safeApply = function(fn) {
        var phase = $rootScope.$$phase;
        if (phase === '$apply' || phase === '$digest') {
            if (fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

});
