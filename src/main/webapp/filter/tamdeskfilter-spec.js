describe('tamdeskfilter', function() {

	beforeEach(module('mytamdesk'));

	it('should ...', inject(function($filter) {

        var filter = $filter('tamdeskfilter');

		expect(filter('input')).toEqual('output');

	}));

});